/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.data

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kent.university.privelt.databinding.CellUserdataBinding
import com.kent.university.privelt.model.UserData
import kotlin.collections.ArrayList

class DataAdapter internal constructor(userData: ArrayList<UserData>, private val isFirstDepth: Boolean,
                                       private val clickCallback: (UserData) -> Unit, private val longClickCallback: (UserData, View) -> Unit
) :
    RecyclerView.Adapter<DataViewHolder>() {

    private var userData: List<UserData>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = CellUserdataBinding.inflate(inflater, parent, false)
        return DataViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            clickCallback(userData[position])
        }
        holder.itemView.setOnLongClickListener {
            longClickCallback(userData[position], it)
            true
        }
        holder.bind(userData[position], isFirstDepth, areItemsTheSame(), position)
    }

    private fun areItemsTheSame(): Boolean {
        return !userData.any { it.subtype != userData[0].subtype } && userData.size > 1
    }

    override fun getItemCount(): Int {
        return userData.size
    }

    fun setUserData(userData: List<UserData>) {
        this.userData = userData
    }

    init {
        this.userData = userData
    }

}
