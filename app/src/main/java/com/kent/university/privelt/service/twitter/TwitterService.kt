package com.kent.university.privelt.service.twitter

import android.content.Context
import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.service.ApiResponseException
import com.kent.university.privelt.service.base.APIServiceListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TwitterService: Service("Twitter", false, "", "", "") {

    lateinit var APIServiceListener: APIServiceListener
    private val redirectUrl = "privelt%3A%2F%2Ftwitter"
    val clientId = "SlltWkFRUVIyYzhnZndERjdLb3k6MTpjaQ"
    val clientSecret = "rFawEXnl-VhOQcm9a-8IEXwU5lCFcZCBacl-8Qqz06UuUQ1oxs"
    var systemTime = System.currentTimeMillis()

    fun insertServiceAndFetchData(context: Context, token: String) = CoroutineScope(Dispatchers.IO).launch {

        CoroutineScope(Dispatchers.Main).launch {
            APIServiceListener.loading()
        }

        systemTime = System.currentTimeMillis()

        val id = insertOrGetService(context)
        getProfile(context, id, token)
    }

    fun connection(context: Context, webView: WebView) {
        openBrowser(webView, context)
    }

    private fun openBrowser(webView: WebView, context: Context) {
        webView.isVisible = true
        webView.settings.javaScriptEnabled = true
        val redirectScheme = "privelt"
        val url = "https://twitter.com/i/oauth2/authorize?response_type=code&client_id=$clientId&redirect_uri=$redirectUrl&scope=tweet.read%20users.read&state=state&code_challenge=challenge&code_challenge_method=plain"
        webView.loadUrl(url)
        // Set Redirect Listener
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                request?.let {
                    // Check if this url is our OAuth redirectUrl, otherwise ignore it
                    if (request.url.toString().startsWith(redirectScheme)) {
                        // This is our request. Parse the redirect URL query parameters to get the code
                        request.url.getQueryParameter("code")?.let { code ->
                            // Got code!
                            Log.d("OAuth code", code)
                            getToken(code, context)
                        } ?: run {
                            // User cancelled the login flow
                            Log.d("OAuth", "Authorization code not received :(")
                            APIServiceListener.error("Authorization code not received :(")

                            val exception = ApiResponseException(
                                "Authorization code not received :(",
                                "",
                                500)
                            Firebase.crashlytics.recordException(exception)
                        }
                        return true
                    }
                }
                return false
            }
        }
    }

    fun getToken(code: String, context: Context) {
        val retTwitter = RetrofitTwitterInstance.getApiRetrofitInstance()
        retTwitter.requestToken(clientId, "authorization_code", "privelt://twitter", "challenge", code)
            .enqueue(object: Callback<AuthRequestTokenResponse> {
                override fun onFailure(call: Call<AuthRequestTokenResponse>, t: Throwable) {
                    Log.e("twitter", t.message.toString())
                    Firebase.crashlytics.recordException(t)
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<AuthRequestTokenResponse>, response: Response<AuthRequestTokenResponse>) {
                    if (response.code() == 200) {
                        Log.d("twitter","Generated token!")
                        response.body()?.access_token?.let { token ->
                            Log.d("Access token", token)
                            insertServiceAndFetchData(context, token)
                        }
                    } else {
                        Log.d("twitter","Token exchange failed!")
                        val exception = ApiResponseException(
                            "Token exchange failed!",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        APIServiceListener.error("Token exchange failed!")
                    }
                }
            })
    }

    private fun getProfile(context: Context, serviceId: Long, token: String) {
        val retroTwitter = RetrofitTwitterInstance.getApiRetrofitInstance()
        val fields = "created_at,protected,withheld,location,url,description,verified"
        val bearerToken = "Bearer $token"
        val userDataRepository = PriVELTDatabase.getInstance(context)?.userDataDao()

        retroTwitter.profile(fields, bearerToken)
            .enqueue(object: Callback<ProfileData> {
                override fun onFailure(call: Call<ProfileData>, t: Throwable) {
                    Log.e("twitter", t.message.toString())
                    Firebase.crashlytics.recordException(t)
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<ProfileData>, response: Response<ProfileData>) {
                    if (response.code() == 200) {
                        Log.d("twitter","Fetched profile!")
                        response.body()?.data?.id.let { it ->
                            if (it != null) {

                                val test = response.body()?.toUserData(serviceId, systemTime)

                                //save username
                                CoroutineScope(Dispatchers.IO).launch {
                                    val serviceDataRepository =
                                        PriVELTDatabase.getInstance(context)?.serviceDao()
                                    val service =
                                        serviceDataRepository?.getServiceWithName(this@TwitterService.name)
                                    service?.user = response.body()?.data?.username ?: ""
                                    serviceDataRepository?.updateServices(service)
                                }

                                CoroutineScope(Dispatchers.IO).launch {
                                    test?.forEach {
                                        if (it.isValid())
                                            userDataRepository?.insertUserData(it)
                                    }
                                }

                                getTweets(context, serviceId, token, it)
                            }
                        }
                    }
                    else{
                        Log.d("twitter", response.message())
                        Log.d("twitter","Fetching profile failed!")
                        val exception = ApiResponseException(
                            "Fetching profile failed!",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        APIServiceListener.error("Fetching profile failed!")
                    }
                }
            })
    }

    private fun getTweets(context: Context, serviceId: Long, token: String, userId: String) {
        val retroTwitter = RetrofitTwitterInstance.getApiRetrofitInstance()
        val fields = "created_at,author_id,conversation_id,in_reply_to_user_id,source,lang,geo,public_metrics,possibly_sensitive"
        val bearerToken = "Bearer $token"
        val userDataRepository = PriVELTDatabase.getInstance(context)?.userDataDao()

        retroTwitter.timeline(userId, fields, bearerToken)
            .enqueue(object: Callback<TweetsData> {
                override fun onFailure(call: Call<TweetsData>, t: Throwable) {
                    Log.e("twitter", t.message.toString())
                    Firebase.crashlytics.recordException(t)
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<TweetsData>, response: Response<TweetsData>) {
                    if (response.code() == 200) {
                        CoroutineScope(Dispatchers.IO).launch {

                            val test = response.body()?.toUserData(serviceId, systemTime)

                            test?.forEach {
                                if (it.isValid())
                                    userDataRepository?.insertUserData(it)
                            }
                        }
                        APIServiceListener.success()
                        Log.d("twitter","Fetched tweets!")
                    }
                    else{
                        Log.d("twitter","Fetching tweets failed!")
                        val exception = ApiResponseException(
                            "Fetching tweets failed!",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        APIServiceListener.error("Fetching tweets failed!")
                    }
                }
            })
    }
}