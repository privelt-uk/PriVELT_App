package com.kent.university.privelt.service.facebook

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.core.view.isVisible
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.model.facebook.FacebookProfile
import com.kent.university.privelt.service.base.APIServiceListener
import com.kent.university.privelt.service.base.DataTypeList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class FacebookService : Service("Facebook", false, "", "", "") {

    lateinit var APIServiceListener: APIServiceListener

    var systemTime: Long = System.currentTimeMillis()

    fun insertServiceAndFetchData(context: Context) = CoroutineScope(Dispatchers.IO).launch {

        CoroutineScope(Dispatchers.Main).launch {
            APIServiceListener.loading()

        }

        systemTime = System.currentTimeMillis()
        val id = insertOrGetService(context)
        getProfile(context, id)
    }

    fun connection(callbackManager: CallbackManager, loginButton: LoginButton) {

        this.registerButtonCallback(callbackManager, loginButton)

        loginButton.isVisible = true
        Log.d("LOGIN", isLoggedIn().toString())
        if (isLoggedIn()) {
            insertServiceAndFetchData(loginButton.context)
        }

        LoginManager.getInstance()
            .registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    // App code
                    insertServiceAndFetchData(loginButton.context)
                }

                override fun onCancel() {
                    // App code
                }

                override fun onError(exception: FacebookException) {
                    // App code
                    Firebase.crashlytics.recordException(exception)
                    APIServiceListener.error("login failed")
                }
            })
    }

    private fun registerButtonCallback(callbackManager: CallbackManager, loginButton: LoginButton) {
        listOf(EMAIL)
        // If you are using in a fragment, call loginButton.setFragment(this);

        // Callback registration
        // If you are using in a fragment, call loginButton.setFragment(this);

        // Callback registration
        loginButton.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                // App code
                insertServiceAndFetchData(loginButton.context)
            }

            override fun onCancel() {
                // App code
            }

            override fun onError(exception: FacebookException) {
                // App code
                Firebase.crashlytics.recordException(exception)
                APIServiceListener.error("login failed")
            }
        })
    }

    private fun isLoggedIn(): Boolean {
        val accessToken = AccessToken.getCurrentAccessToken()
        return accessToken != null
    }

    private fun getProfile(context: Context, id: Long) {

        val userDataRepository = PriVELTDatabase.getInstance(context)?.userDataDao()

        val bundle = Bundle()
        bundle.putString(
            "fields",
            "id,about,age_range,email,first_name,last_name,location,name,relationship_status,significant_other,timezone,work,website"
        )
        /* make the API call */
        GraphRequest(
            AccessToken.getCurrentAccessToken(),
            "/me",
            bundle,
            HttpMethod.GET,
            { response -> /* handle the result */
                Log.d("DEBUG", "Response from Profile")
                Log.d("RESPONSE", response.jsonObject.toString())
                val profile =
                    Gson().fromJson(response.jsonObject.toString(), FacebookProfile::class.java)

                //save username
                CoroutineScope(Dispatchers.IO).launch {
                    val serviceDataRepository =
                        PriVELTDatabase.getInstance(context)?.serviceDao()
                    val service =
                        serviceDataRepository?.getServiceWithName(this@FacebookService.name)
                    service?.user = profile.email ?: ""
                    serviceDataRepository?.updateServices(service)
                }

                profile.email
                val allData = mutableListOf<UserData>()
                val initialUserData = UserData(title = "Facebook Profile",
                    type = DataTypeList.PROFILE.title,
                    subtype = "Facebook Profile",
                    value = "",
                    serviceId = id,
                    date = systemTime)
                allData.add(initialUserData)
                val userData = profile.toUserData(id, systemTime, initialUserData.id)
                allData.addAll(userData)
                CoroutineScope(Dispatchers.IO).launch {
                    allData.forEach {
                        if (it.isValid())
                            userDataRepository?.insertUserData(it)
                    }
                }

                Log.d("DEBUG", "Response from Profile")
                Log.d("PARSE", userData.toString())
                APIServiceListener.success()
            }
        ).executeAsync()
    }

    companion object {
        private const val EMAIL = "email"
    }

}
