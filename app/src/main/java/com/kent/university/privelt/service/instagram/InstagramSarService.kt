package com.kent.university.privelt.service.instagram

import android.content.Context
import com.google.gson.Gson
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.instagram.InstagramLinkedMetaAccounts
import com.kent.university.privelt.model.instagram.InstagramPersonalInformation
import com.kent.university.privelt.service.base.SARService
import com.kent.university.privelt.service.base.State
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.InputStream
import java.util.zip.ZipInputStream

class InstagramSarService: SARService() {

    override val sarRequestURL: String = "https://help.instagram.com/181231772500920"
    override val service: Service = Service("Instagram SAR", false, "", "", "")

    override fun parseSAR(context: Context, inputStream: InputStream) =
        CoroutineScope(Dispatchers.IO).launch {
            _stateFlow.emit(State.Loading)
            val date = System.currentTimeMillis()
            val zipInputStream = ZipInputStream(inputStream)
            val pairs = zipInputStream.use { zipStream ->
                generateSequence { zipStream.nextEntry }
                    .filter { it.name.endsWith(".json") }
                    .filterNot { it.name.startsWith("__") }
                    .map {
                        Pair(it.name, zipStream.reader().readText())
                    }
                    .toList()
            }
            val gson = Gson()
            val serviceId = 0L
            if (pairs.isEmpty()) {
                _stateFlow.emit(State.Error("The selected file is not a SAR file for this service"))
                return@launch
            }
            pairs.map {
                if (it.first.contains("linked_meta_accounts.json")) {
                    val userData = gson.fromJson(it.second, InstagramLinkedMetaAccounts::class.java).toUserData(serviceId, date)
                    saveUserDataToRepository(context, userData)
                } else if (it.first.contains("personal_information.json")) {
                    val userData =
                        gson.fromJson(it.second, InstagramPersonalInformation::class.java)
                            .toUserData(serviceId, date)
                    saveUserDataToRepository(context, userData, true)
                }
            }
        }
}