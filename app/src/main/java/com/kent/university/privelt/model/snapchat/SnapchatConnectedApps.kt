package com.kent.university.privelt.model.snapchat

import com.google.gson.annotations.SerializedName
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class SnapchatConnectedApps(
    @SerializedName("Connected Applications")
    val connectedApplications: List<SnapchatConnectedApplication>?
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val initialUserData = UserData(title = "Snapchat Connected Apps",
            type = DataTypeList.PROFILE.title,
            subtype = "Snapchat Connected Apps",
            value = "",
            serviceId = serviceId,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)

        val friendRequestsSent: List<UserData> = connectedApplications?.flatMap{ it.toUserData(serviceId, date, initialUserData.id) } ?: emptyList()
        allData.addAll(friendRequestsSent)
        return allData
    }
}

data class SnapchatConnectedApplication(
    @SerializedName("App")
    val app: String?,
    @SerializedName("App Status")
    val appStatus: String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val allData = mutableListOf<UserData>()
        val appUserData = UserData(title = "Snapchat Connected App",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.CONNECTED_APPS.value,
            value = app ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(appUserData)

        val appStatusUserData = UserData(title = "Snapchat Connected App Status",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.CONNECTED_APPS.value,
            value = appStatus ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(appStatusUserData)
        return allData
    }
}
