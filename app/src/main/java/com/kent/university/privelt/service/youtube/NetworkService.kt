package com.kent.university.privelt.service.youtube

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataTypeList
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import kotlin.reflect.full.declaredMemberProperties

interface ApiInterface {

    @FormUrlEncoded
    @POST("token")
    fun refreshToken(
        @Field("grant_type") grantType: String,
        @Field("client_id") clientId: String,
        @Field("refresh_token") refreshToken: String
    ): Call<AuthRequestTokenResponse>

    @FormUrlEncoded
    @POST("token")
    fun requestToken(
        @Field("grant_type") grantType: String,
        @Field("client_id") clientId: String,
        @Field("code") code: String,
        @Field("redirect_uri") redirectUri: String,
        @Field("code_verifier") codeVerifier: String
    ): Call<AuthRequestTokenResponse>

    @GET("youtube/v3/playlists")
    fun requestPlaylists(
        @Header("Authorization") authorize: String,
        @Query("part") part: String,
        @Query("mine") mine: Boolean,
        @Query("maxResults") maxResults: String
    ): Call<YoutubePlaylistList>

    @GET("youtube/v3/subscriptions")
    fun requestSubscriptions(
        @Header("Authorization") authorize: String,
        @Query("part") part: String,
        @Query("mine") mine: Boolean,
        @Query("maxResults") maxResults: String
    ): Call<YoutubeSubscriptionList>
}

class RetrofitYoutubeInstance {
    companion object {
        private const val BASE_AUTH_URL: String = "https://oauth2.googleapis.com/"
        private const val BASE_API_URL: String = "https://www.googleapis.com/"

        fun getAuthRetrofitInstance(): ApiInterface {
            return Retrofit.Builder()
                .baseUrl(BASE_AUTH_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiInterface::class.java)
        }

        fun getApiRetrofitInstance(): ApiInterface {
            return Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiInterface::class.java)
        }
    }
}

data class AuthRequestTokenResponse(val access_token: String,
                                    val token_type: String,
                                    val id_token: String,
                                    val scope: String,
                                    val expires_in: Int,
                                    val refresh_token: String)

data class YoutubePlaylistList(val kind: String?,
                               val etag: String?,
                               val nextPageToken: String?,
                               val nextSyncToken: String?,
                               val pageInfo: YoutubePageInfo?,
                               val items: List<YoutubeItem>?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val filteredFields = declaredFields.filterNot {
            it.name == "pageInfo" && it.name == "items"
        }
        val allData = mutableListOf<UserData>()
        val userDataList = filteredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Youtube $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        allData.addAll(userDataList)
        val pageInfoUserDetails = pageInfo?.toUserData(serviceId, date, parentId) ?: emptyList()
        val itemsUserData = items?.flatMap { it.toUserData(serviceId, date, parentId) } ?: emptyList()
        allData.addAll(pageInfoUserDetails)
        allData.addAll(itemsUserData)
        return allData
    }
}

data class YoutubeSubscriptionList(val kind: String?,
                                   val etag: String?,
                                   val nextPageToken: String?,
                                   val nextSyncToken: String?,
                                   val pageInfo: YoutubePageInfo?,
                                   val items: List<YoutubeSubscriptionItem>?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val filteredFields = declaredFields.filterNot {
            it.name == "pageInfo" && it.name == "items"
        }
        val allData = mutableListOf<UserData>()
        val userDataList = filteredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Youtube $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        allData.addAll(userDataList)
        val pageInfoUserDetails = pageInfo?.toUserData(serviceId, date, parentId) ?: emptyList()
        val itemsUserData = items?.flatMap { it.toUserData(serviceId, date, parentId) } ?: emptyList()
        allData.addAll(pageInfoUserDetails)
        allData.addAll(itemsUserData)
        return allData
    }
}

data class YoutubeItem(val kind: String?,
                       val etag: String?,
                       val id: String?,
                       val snippet: YoutubeItemSnippet?,
                       val status: YoutubeItemStatus?,
                       val contentDetails: YoutubeContentDetails?,
                       val player: YoutubeItemPlayer?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val filteredFields = declaredFields.filterNot {
            it.name == "snippet" && it.name == "status" && it.name == "contentDetails" && it.name == "player"
        }
        val allData = mutableListOf<UserData>()
        val userDataList = filteredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Youtube $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        allData.addAll(userDataList)
        val snippetUserDetails = snippet?.toUserData(serviceId, date, parentId) ?: emptyList()
        val statusUserData = status?.toUserData(serviceId, date, parentId) ?: emptyList()
        val contentDetailsUserData = contentDetails?.toUserData(serviceId, date, parentId) ?: emptyList()
        val playerUserData = player?.toUserData(serviceId, date, parentId) ?: emptyList()
        allData.addAll(snippetUserDetails)
        allData.addAll(statusUserData)
        allData.addAll(contentDetailsUserData)
        allData.addAll(playerUserData)
        return allData
    }
}

data class YoutubeSubscriptionItem(val kind: String?,
                                   val etag: String?,
                                   val id: String?,
                                   val snippet: YoutubeSubscriptionSnippet?,
                                   val contentDetails: YoutubeSubscriptionContentDetails?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val filteredFields = declaredFields.filterNot {
            it.name == "snippet" && it.name == "contentDetails"
        }
        val allData = mutableListOf<UserData>()
        val userDataList = filteredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Youtube $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        allData.addAll(userDataList)
        val snippetUserDetails = snippet?.toUserData(serviceId, date, parentId) ?: emptyList()
        val contentDetailsUserData = contentDetails?.toUserData(serviceId, date, parentId) ?: emptyList()
        allData.addAll(contentDetailsUserData)
        allData.addAll(snippetUserDetails)
        return allData
    }
}

data class YoutubeItemSnippet(val publishedAt: String?,
                              val channelId: String?,
                              val title: String?,
                              val description: String?,
                              val thumbnails: YoutubeItemThumbnails?,
                              val channelTitle: String?,
                              val localized: YoutubeItemLocalized?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val filteredFields = declaredFields.filterNot {
            it.name == "thumbnails" && it.name == "localized"
        }
        val allData = mutableListOf<UserData>()
        val userDataList = filteredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Youtube $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        allData.addAll(userDataList)
        val thumbnailsUserDetails = thumbnails?.toUserData(serviceId, date, parentId) ?: emptyList()
        val localizedDetailsUserData = localized?.toUserData(serviceId, date, parentId) ?: emptyList()
        allData.addAll(thumbnailsUserDetails)
        allData.addAll(localizedDetailsUserData)
        return allData
    }
}

data class YoutubeItemThumbnails(val default: YoutubeThumbnail?,
                                 val medium: YoutubeThumbnail?,
                                 val high: YoutubeThumbnail?,
                                 val standard: YoutubeThumbnail?,
                                 val maxres: YoutubeThumbnail?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val allData = mutableListOf<UserData>()
        val defaultUserDetails = default?.toUserData(serviceId, date, parentId) ?: emptyList()
        val mediumDetailsUserData = medium?.toUserData(serviceId, date, parentId) ?: emptyList()
        val highUserDetails = high?.toUserData(serviceId, date, parentId) ?: emptyList()
        val standardDetailsUserData = standard?.toUserData(serviceId, date, parentId) ?: emptyList()
        val maxresDetailsUserData = maxres?.toUserData(serviceId, date, parentId) ?: emptyList()
        allData.addAll(defaultUserDetails)
        allData.addAll(mediumDetailsUserData)
        allData.addAll(highUserDetails)
        allData.addAll(standardDetailsUserData)
        allData.addAll(maxresDetailsUserData)
        return allData
    }
}

data class YoutubeThumbnail(val url: String?,
                            val width: Int?,
                            val height: Int?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Youtube $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}

data class YoutubeContentDetails(val itemCount: Int?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Youtube $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}

data class YoutubeSubscriptionContentDetails(val totalItemCount: Int?,
                                             val newItemCount: Int?,
                                             val activityType: String?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Youtube $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}

data class YoutubeItemStatus(val privacyStatus: String?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Youtube $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}

data class YoutubeItemPlayer(val privacyStatus: String?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Youtube $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}

data class YoutubeItemLocalized(val title: String?,
                                val description: String?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Youtube $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}

data class YoutubePageInfo(val totalResults: Int?,
                           val resultsPerPage: Int?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Youtube $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}

data class YoutubeSubscriptionSnippet(val publishedAt: String?,
                                      val channelId: String?,
                                      val title: String?,
                                      val description: String?,
                                      val thumbnails: YoutubeItemThumbnails?,
                                      val resourceId: YoutubeSubscriptionResource?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val filteredFields = declaredFields.filterNot {
            it.name == "thumbnails" && it.name == "resourceId"
        }
        val allData = mutableListOf<UserData>()
        val userDataList = filteredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Youtube $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        val thumbnailsUserDetails = thumbnails?.toUserData(serviceId, date, parentId) ?: emptyList()
        val resourceIdUserData = resourceId?.toUserData(serviceId, date, parentId) ?: emptyList()
        allData.addAll(thumbnailsUserDetails)
        allData.addAll(resourceIdUserData)
        allData.addAll(userDataList)
        return allData
    }
}

data class YoutubeSubscriptionResource(val kind: String?,
                                       val channelId: String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Youtube $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}
