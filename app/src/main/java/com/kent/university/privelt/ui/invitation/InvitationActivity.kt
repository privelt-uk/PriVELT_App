package com.kent.university.privelt.ui.invitation

import android.app.Service
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.core.text.isDigitsOnly
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import com.kent.university.privelt.R
import com.kent.university.privelt.base.BaseActivity
import com.kent.university.privelt.databinding.ActivityInvitationBinding
import com.kent.university.privelt.ui.master_password.FirstScreenActivity
import com.kent.university.privelt.ui.master_password.MasterPasswordActivity

class InvitationActivity : BaseActivity() {
    override val activityLayout: Int
        get() = R.layout.activity_invitation

    private val binding by lazy { ActivityInvitationBinding.inflate(layoutInflater) }

    override fun configureViewModel() {

    }

    override fun configureDesign(savedInstanceState: Bundle?) = with(binding) {
        setContentView(root)
        val masterPasswordAlreadyGiven = identityManager?.isInvitationCodeAlreadyGiven()
        if (masterPasswordAlreadyGiven == true)
            startActivity(Intent(this@InvitationActivity, MasterPasswordActivity::class.java))

        editTextCode.showKeyboard()
        editTextCode.requestFocus()

        content.isVisible = true
        content.alpha = 1f

       start.setOnClickListener {
           sendCodeTriggered()
       }
    }

    private fun sendCodeTriggered() = with(binding) {
        val code: String = editTextCode.text.toString()

        if (code == "priVelt@2023") {
            identityManager?.setInvitationCode()
            startActivity(Intent(this@InvitationActivity, FirstScreenActivity::class.java))
        }
        else
            Toast.makeText(this@InvitationActivity, "Wrong invitation code", Toast.LENGTH_LONG).show()
    }

    private fun View.showKeyboard() {
        if (requestFocus()) {
            val imm = context.getSystemService(Service.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(this, 0)
        }
    }
}