package com.kent.university.privelt.model.uber

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListDevice
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class RiderAppAnalytics(
    val latitude: String,
    val longitude: String,
    val city: String,
    val cellularCarrier: String,
    val carrierMcc: String,
    val carrierMnc: String,
    val ipAddress: String,
    val deviceLanguage: String,
    val deviceModel: String,
    val deviceOs: String,
    val deviceOsVersion: String,
    val region: String
){
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Uber rider app analytics Data $title",
                type = DataTypeList.DEVICE.title,
                subtype = getDeviceSubType(field.name),
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }

    private fun getDeviceSubType(fieldName: String): String {
        return when {
            fieldName.contains("ipAddress") -> DataSubTypeListDevice.IP.value
            fieldName.contains("deviceOs") -> DataSubTypeListDevice.OS.value
            fieldName.contains("deviceOsVersion") -> DataSubTypeListDevice.OS.value
            fieldName.contains("deviceModel") -> DataSubTypeListDevice.DEVICE_TYPE.value
            else -> fieldName.replace("_", " ")
        }
    }
}