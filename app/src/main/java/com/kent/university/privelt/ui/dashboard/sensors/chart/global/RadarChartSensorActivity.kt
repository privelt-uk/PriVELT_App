/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.kent.university.privelt.ui.dashboard.sensors.chart.global

import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.RadarData
import com.github.mikephil.charting.data.RadarDataSet
import com.github.mikephil.charting.data.RadarEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet
import com.kent.university.privelt.R
import com.kent.university.privelt.base.BaseActivity
import com.kent.university.privelt.databinding.ActivityRiskValuePermissionBinding
import com.kent.university.privelt.model.Sensor
import com.kent.university.privelt.utils.getRandomColor
import java.util.*

class RadarChartSensorActivity : BaseActivity() {

    private lateinit var binding: ActivityRiskValuePermissionBinding

    override fun configureDesign(savedInstanceState: Bundle?) {
        binding = ActivityRiskValuePermissionBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.chart.visibility = View.GONE
        binding.noData.visibility = View.VISIBLE
        val sensors = Sensor.values()
        configureChart(sensors)
    }

    override val activityLayout: Int
        get() = R.layout.activity_risk_value_permission

    override fun configureViewModel() {
    }

    private fun configureChart(sensors: Array<Sensor>) {
        binding.chart.visibility = View.VISIBLE
        binding.noData.visibility = View.GONE
        binding.chart.description.isEnabled = false
        binding.chart.webLineWidth = 1f
        binding.chart.webColor = Color.LTGRAY
        binding.chart.webLineWidthInner = 1f
        binding.chart.webColorInner = Color.LTGRAY
        binding.chart.webAlpha = 100

        setData(sensors)
        binding.chart.animateXY(1400, 1400, Easing.EaseInOutQuad)
        val xAxis = binding.chart.xAxis
        xAxis.textSize = 9f
        xAxis.yOffset = 0f
        xAxis.xOffset = 0f
        xAxis.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return if (value.toInt() < Sensor.values().size.toFloat()) sensors[value.toInt()].title else ""
            }
        }
    }

    private fun setData(sensors: Array<Sensor>) {
        val sets: MutableList<IRadarDataSet> = ArrayList()
        val entries = ArrayList<RadarEntry>()
        for (sensor: Sensor in sensors) {
            val `val` = sensor.getApplications().size
            entries.add(RadarEntry(`val`.toFloat()))
        }
        binding.heading.text = getString(R.string.chart_spider_heading_permissions, entries.size)
        val set1 = RadarDataSet(entries, "Permissions")
        Random()
        val color = getRandomColor()
        set1.color = color
        set1.fillColor = color
        set1.setDrawFilled(true)
        set1.fillAlpha = 180
        set1.lineWidth = 2f
        set1.isDrawHighlightCircleEnabled = true
        set1.setDrawHighlightIndicators(false)
        sets.add(set1)
        val data = RadarData(sets)
        data.setValueTextSize(15f)
        data.setDrawValues(false)
        data.setValueTextColor(Color.RED)
        val yAxis = binding.chart.yAxis
        yAxis.setLabelCount(1, false)
        yAxis.textSize = 9f
        yAxis.axisMinimum = 0f
        yAxis.axisMaximum =
            sensors.maxByOrNull { it.getApplications().size }?.getApplications()?.size?.toFloat()!!
        yAxis.setDrawLabels(true)
        val l = binding.chart.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        l.orientation = Legend.LegendOrientation.HORIZONTAL
        l.setDrawInside(false)
        l.xEntrySpace = 7f
        l.yEntrySpace = 5f
        binding.chart.data = data
        binding.chart.invalidate()
    }

}
