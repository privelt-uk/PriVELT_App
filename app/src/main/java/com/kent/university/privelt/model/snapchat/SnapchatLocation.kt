package com.kent.university.privelt.model.snapchat

import com.google.gson.annotations.SerializedName
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataSubtypeList
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class SnapchatLocationData(
    @SerializedName("Latest Location")
    val latestLocation: List<LatestLocation>?,
    @SerializedName("Home & Work")
    val homeAndWork: HomeAndWork?,
    @SerializedName("Areas you may have visited in the last two years")
    val areasVisitedLastTwoYears: List<AreaVisited>?
) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val initialUserData = UserData(title = "Snapchat Location History",
            type = DataTypeList.LOCATION.title,
            subtype = "Snapchat Location History",
            value = "",
            serviceId = serviceId,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)

        val friends: List<UserData> = latestLocation?.flatMap{ it.toUserData(serviceId, date, initialUserData.id) } ?: emptyList()
        val friendRequestsSent: List<UserData> = homeAndWork?.toUserData(serviceId, date, initialUserData.id) ?: emptyList()
        val deletedFriends: List<UserData> = areasVisitedLastTwoYears?.flatMap{ it.toUserData(serviceId, date, initialUserData.id) } ?: emptyList()
        allData.addAll(friends)
        allData.addAll(friendRequestsSent)
        allData.addAll(deletedFriends)
        return allData
    }
}

data class LatestLocation(
    @SerializedName("City")
    val city: String?,
    @SerializedName("Country")
    val country: String?,
    @SerializedName("Region")
    val region: String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val allData = mutableListOf<UserData>()

        val cityUserData = UserData(title = "Snapchat Location History City",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = city ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(cityUserData)

        val countryUserData = UserData(title = "Snapchat Location History Country",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = country ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(countryUserData)

        val regionUserData = UserData(title = "Snapchat Location History Region",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = region ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(regionUserData)

        return allData
    }
}

data class HomeAndWork(
    @SerializedName("Home")
    val home: LatestLocation?,
    @SerializedName("Work")
    val work: LatestLocation?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val allData = mutableListOf<UserData>()
        val homeLocation = home?.toUserData(serviceId, date, parentId) ?: emptyList()
        val workLocation = work?.toUserData(serviceId, date, parentId) ?: emptyList()
        allData.addAll(homeLocation)
        allData.addAll(workLocation)
        return allData
    }
}

data class AreaVisited(
    @SerializedName("Time")
    val time: String?,
    @SerializedName("City")
    val city: String?,
    @SerializedName("Region")
    val region: String?,
    @SerializedName("Postal Code")
    val postalCode: String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val allData = mutableListOf<UserData>()

        val cityUserData = UserData(title = "Snapchat Location Visited City",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = city ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(cityUserData)

        val regionUserData = UserData(title = "Snapchat Location Visited Region",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = region ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(regionUserData)

        val postalCodeUserData = UserData(title = "Snapchat Location Visited Postal Code",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = postalCode ?: "",
            serviceId = serviceId,
            date = date,
            parentId = parentId)
        allData.add(postalCodeUserData)

        val dateInitialUserData = UserData(title = "Snapchat Location Visited Date",
            type = DataTypeList.OTHER.title,
            subtype = "",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(dateInitialUserData)

        val timeUserData = UserData(title = "Snapchat Location Visited Date",
            type = DataTypeList.OTHER.title,
            subtype = "Time Visited",
            value = time ?: "",
            serviceId = serviceId,
            date = date,
            parentId = dateInitialUserData.id)
        allData.add(timeUserData)

        return allData
    }
}
