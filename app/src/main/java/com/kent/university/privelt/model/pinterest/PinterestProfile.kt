package com.kent.university.privelt.model.pinterest

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class PinterestProfile(
    val username : String,
    val accountCreationTime : String,
    val firstName : String,
    val lastName : String,
    val about : String,
    val location : String,
    val website : String,
    val email : String,
    val emailVerified : String,
    val gender : String,
    val country : String,
    val birthday : String,
    val hideProfileFromSearchEngines : String,
    val shareActivity : String,
    val improveRecommendations : String,
    val improveRecommendationsPartners : String,
    val improveAds : String,
    val emailEngagement : String,
    val hasPinterestExtension : String,
    val interestCount : String,
    val clickthroughCount : String,
    val searchCount : String
) {
    fun toUserData(serviceId: Long, date: Long ): List<UserData> {
        val initialUserData = UserData(title = "Pinterest Profile",
            type = DataTypeList.PROFILE.title,
            subtype = "Pinterest Profile",
            value = "",
            serviceId = 0L,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)

        val usernameUserData = UserData(title = "Pinterest profile Username",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.USERNAME.value,
            value = username,
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(usernameUserData)

        val accountCreationTimeUserData = UserData(title = "Pinterest profile Account creation time",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Account creation time: $accountCreationTime",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(accountCreationTimeUserData)

        val firstNameUserData = UserData(title = "Pinterest profile First Name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = firstName,
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(firstNameUserData)

        val lastNameUserData = UserData(title = "Pinterest profile Last Name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = lastName,
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(lastNameUserData)

        val aboutUserData = UserData(title = "Pinterest profile About",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "About: $about",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(aboutUserData)

        val initialLocationUserData = UserData(title = "Pinterest Profile Location",
            type = DataTypeList.LOCATION.title,
            subtype = "Pinterest Profile Location",
            value = "",
            serviceId = 0L,
            date = date)
        allData.add(initialLocationUserData)
        val locationUserData = UserData(title = "Pinterest profile Location details",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = location,
            serviceId = serviceId,
            date = date,
            parentId = initialLocationUserData.id)
        allData.add(locationUserData)

        val websiteUserData = UserData(title = "Pinterest profile Website",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.WEBSITE.value,
            value = website,
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(websiteUserData)

        val emailUserData = UserData(title = "Pinterest profile Email",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.EMAIL.value,
            value = email,
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(emailUserData)

        val emailVerifiedUserData = UserData(title = "Pinterest profile Email verified",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Email Verified: $emailVerified",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(emailVerifiedUserData)

        val genderUserData = UserData(title = "Pinterest profile Gender",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.GENDER.value,
            value = gender,
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(genderUserData)

        val countryUserData = UserData(title = "Pinterest profile Country",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = country,
            serviceId = serviceId,
            date = date,
            parentId = initialLocationUserData.id)
        allData.add(countryUserData)

        val birthdayUserData = UserData(title = "Pinterest profile Birthday",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.AGE_INFO.value,
            value = birthday,
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(birthdayUserData)

        val hideProfileFromSearchEnginesUserData = UserData(title = "Pinterest profile Hide from search engines",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Hide profile from search engines: $hideProfileFromSearchEngines",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(hideProfileFromSearchEnginesUserData)

        val shareActivityUserData = UserData(title = "Pinterest profile Share Activity",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Share Activity: $shareActivity",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(shareActivityUserData)

        val improveRecommendationsUserData = UserData(title = "Pinterest profile Improve Recommendations",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Improve Recommendations: $improveRecommendations",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(improveRecommendationsUserData)

        val improveRecommendationsPartnersUserData = UserData(title = "Pinterest profile Improve Recommendations Partners",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Improve Recommendations Partners: $improveRecommendationsPartners",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(improveRecommendationsPartnersUserData)

        val improveAdsUserData = UserData(title = "Pinterest profile Improve Ads",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Improve Ads: $improveAds",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(improveAdsUserData)

        val emailEngagementUserData = UserData(title = "Pinterest profile Email Engagement",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Email Engagement: $emailEngagement",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(emailEngagementUserData)

        val hasPinterestExtensionUserData = UserData(title = "Pinterest profile Has Pinterest Extension",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Has Pinterest Extension: $hasPinterestExtension",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(hasPinterestExtensionUserData)

        val interestCountUserData = UserData(title = "Pinterest profile Interest Count",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Interest Count: $interestCount",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(interestCountUserData)

        val clickthroughCountUserData = UserData(title = "Pinterest profile Clickthrough Count",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Interest Count: $clickthroughCount",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(clickthroughCountUserData)

        val searchCountUserData = UserData(title = "Pinterest profile Search Count",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Search Count: $searchCount",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(searchCountUserData)
        return allData
    }
}