/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.risk_value

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.RadarData
import com.github.mikephil.charting.data.RadarDataSet
import com.github.mikephil.charting.data.RadarEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.IRadarDataSet
import com.github.mikephil.charting.listener.ChartTouchListener
import com.github.mikephil.charting.listener.OnChartGestureListener
import com.github.mikephil.charting.utils.MPPointD
import com.kent.university.privelt.R
import com.kent.university.privelt.base.BaseActivity
import com.kent.university.privelt.databinding.ActivityRiskValueServiceBinding
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListActivity
import com.kent.university.privelt.service.base.DataSubTypeListDevice
import com.kent.university.privelt.service.base.DataSubTypeListFinancials
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import java.util.*


class RiskValueActivity : BaseActivity() {

    private lateinit var binding: ActivityRiskValueServiceBinding

    private var riskValueViewModel: RiskValueViewModel? = null
    private var services: List<Service>? = null
    private var userData: List<UserData>? = null
    private var type: String? = null
    private var subTypeParent: String? = null
    private var service: String? = null
    private var isDataCentric = false

    override fun configureDesign(savedInstanceState: Bundle?) {
        binding = ActivityRiskValueServiceBinding.inflate(layoutInflater)
        setContentView(binding.root)
        services = ArrayList()
        userData = ArrayList()
        if (savedInstanceState != null) {
            type = savedInstanceState.getString(PARAM_DATA)
            subTypeParent = savedInstanceState.getString(PARAM_SUB_TYPE_PARENT)
            service = savedInstanceState.getString(PARAM_SERVICE)
            isDataCentric = savedInstanceState.getBoolean(PARAM_IS_DATA_CENTRIC)
        } else if (intent != null) {
            type = intent.getStringExtra(PARAM_DATA)
            subTypeParent = intent.getStringExtra(PARAM_SUB_TYPE_PARENT)
            service = intent.getStringExtra(PARAM_SERVICE)
            isDataCentric = intent.getBooleanExtra(PARAM_IS_DATA_CENTRIC, false)
        }
        binding.chart.visibility = View.GONE
        binding.noData.visibility = View.VISIBLE
        getServices()
        userdatas
    }

    private fun configureRecyclerView(services: List<Service>) {
        binding.listCards.layoutManager = GridLayoutManager(this, 3, GridLayoutManager.HORIZONTAL, false)
        val listCards = mutableListOf<String>()
        services.forEach {
            listCards.add(it.name)
        }
        val adapter = CardChartAdapter(listCards, listOf()) { result ->
            val data = services.filter { result.contains(it.name) }
            updateServices(
                data
            )
        }
        binding.listCards.adapter = adapter
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(PARAM_SERVICE, service)
        outState.putString(PARAM_DATA, type)
        outState.putString(PARAM_SUB_TYPE_PARENT, subTypeParent)
        outState.putBoolean(PARAM_IS_DATA_CENTRIC, isDataCentric)
    }

    override val activityLayout: Int
        get() = R.layout.activity_risk_value_service

    override fun configureViewModel() {
        riskValueViewModel = getViewModel(RiskValueViewModel::class.java)
        riskValueViewModel?.init()
    }

    private fun updateUserData(userData: List<UserData>) {
        this.userData = userData
        if (this.userData!!.isNotEmpty() && services!!.isNotEmpty()) configureChart()
    }

    private fun updateServices(services: List<Service>) {
        this.services = services
        if (userData!!.isNotEmpty() && services.isNotEmpty()) configureChart()
    }

    private fun getServices() {
        riskValueViewModel!!.services!!.observe(this) { services: List<Service> ->
            if (!isDataCentric)
                configureRecyclerView(services)
            else
                updateServices(
                    services
                )
        }
    }

    private val userdatas: Unit
        get() {
            riskValueViewModel!!.userDatas?.observe(this) { userData: List<UserData> ->
                if (isDataCentric) {
                    binding.listCards.layoutManager = GridLayoutManager(this, 3, GridLayoutManager.HORIZONTAL, false)
                    val types = userData.map { it.type }.distinct()
                    val adapter = CardChartAdapter(types, listOf()) { result ->
                        val data = userData.filter { result.contains(it.type) }
                        updateUserData(
                            data
                        )
                    }
                    binding.listCards.adapter = adapter
                }
                else
                    updateUserData(
                        userData
                    )
            }
        }

    private fun configureChart() {
        binding.chart.visibility = View.VISIBLE
        binding.noData.visibility = View.GONE
        binding.chart.description.isEnabled = false
        binding.chart.webLineWidth = 1f
        binding.chart.webColor = Color.LTGRAY
        binding.chart.webLineWidthInner = 1f
        binding.chart.webColorInner = Color.LTGRAY
        binding.chart.webAlpha = 100

        val mActivities = numberOfTypes
        setData(mActivities)
        binding.chart.animateXY(1400, 1400, Easing.EaseInOutQuad)
        val xAxis = binding.chart.xAxis
        xAxis.textSize = 9f
        xAxis.yOffset = 0f
        xAxis.xOffset = 0f
        xAxis.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return if (!isDataCentric) mActivities[value.toInt() % mActivities.size] else services!![value.toInt() % services!!.size].name
            }
        }
    }

    private val numberOfTypes: Array<String>
        get() {
            val types: MutableSet<String> = HashSet()
            for ((_, type1) in userData!!) {
                types.add(type1)
            }
            return when (subTypeParent) {
                DataTypeList.PROFILE.title -> DataSubTypeListProfile.values().map { it.value }.toTypedArray()
                DataTypeList.LOCATION.title -> DataSubTypeListLocation.values().map { it.value }.toTypedArray()
                DataTypeList.FINANCIALS.title -> DataSubTypeListFinancials.values().map { it.value }.toTypedArray()
                DataTypeList.DEVICE.title -> DataSubTypeListDevice.values().map { it.value }.toTypedArray()
                DataTypeList.ACTIVITY.title -> DataSubTypeListActivity.values().map { it.value }.toTypedArray()
                else -> return types.toTypedArray()
            }
        }

    private fun setData(mActivities: Array<String>) {
        val sets: MutableList<IRadarDataSet> = ArrayList()

        val adapter = binding.listCards.adapter as CardChartAdapter
        // Looking if we draw a data centric graph or a service centric graph
        if (!isDataCentric) {
            for (service in services!!) {
                if (service.name == this.service || this.service == null || this.service!!.isEmpty()) {
                    val set1 = RadarDataSet(
                        getDataEntriesForEachService(mActivities, service),
                        service.name
                    )
                    val color = adapter.getColor(service.name)
                    set1.color = color
                    set1.fillColor = color
                    set1.setDrawFilled(true)
                    set1.fillAlpha = 180
                    set1.lineWidth = 2f
                    set1.isDrawHighlightCircleEnabled = true
                    set1.setDrawHighlightIndicators(false)
                    sets.add(set1)
                }
            }
        } else {
            for (type in mActivities) {
                if (type == this.type || this.type == null || this.type!!.isEmpty()) {
                    val set1 = RadarDataSet(getDataEntriesForEachType(type, services), type)
                    val color = adapter.getColor(type)
                    set1.color = color
                    set1.fillColor = color
                    set1.setDrawFilled(true)
                    set1.fillAlpha = 180
                    set1.lineWidth = 2f
                    set1.isDrawHighlightCircleEnabled = true
                    set1.setDrawHighlightIndicators(false)
                    sets.add(set1)
                }
            }
        }

        val mode =
            when {
                (services!!.size < 2) and (sets.size < 2) -> 1
                (services!!.size < 2) and (sets.size > 1) -> 2
                (services!!.size > 1) and (sets.size < 2) -> 0
                else -> 4
            }

        binding.heading.text = resources.getQuantityString(
            R.plurals.chart_spider_heading_data,
            mode,
            mActivities.size,
            services!!.size
        )
        val data = RadarData(sets)
        data.setValueTextSize(15f)
        data.setDrawValues(false)
        data.setValueTextColor(Color.RED)
        val yAxis = binding.chart.yAxis
        if (!isDataCentric) yAxis.setLabelCount(1, false) else yAxis.setLabelCount(
            1,
            false
        )
        yAxis.textSize = 9f
        yAxis.axisMinimum = 0f

        yAxis.axisMaximum = getMaximumValue(sets)
        yAxis.setDrawLabels(true)
        val l = binding.chart.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        l.orientation = Legend.LegendOrientation.HORIZONTAL
        l.setDrawInside(false)
        l.xEntrySpace = 7f
        l.yEntrySpace = 5f
        l.isEnabled = false
        binding.chart.data = data
        binding.chart.invalidate()
    }

    private fun getMaximumValue(sets: List<IRadarDataSet>): Float {
        var max = 0f
        for (set in sets) {
            for (i in 0 until set.entryCount) {
                if (max < set.getEntryForIndex(i).value) {
                    max = set.getEntryForIndex(i).value
                }
            }
        }
        return max
    }

    private fun getDataEntriesForEachService(
        mActivities: Array<String>,
        service: Service
    ): List<RadarEntry> {
        val entries = ArrayList<RadarEntry>()
        for (mActivity in mActivities) {
            val `val` = countTypeForEachService(mActivity, service) + 1
            entries.add(RadarEntry(`val`.toFloat()))
        }
        return entries
    }

    private fun getDataEntriesForEachType(
        type: String,
        services: List<Service>?
    ): List<RadarEntry> {
        val entries = ArrayList<RadarEntry>()
        for (service in services!!) {
            val `val` = countServiceForEachType(type, service) + 1
            entries.add(RadarEntry(`val`.toFloat()))
        }
        return entries
    }

    private fun countTypeForEachService(mActivity: String, service: Service): Int {
        var count = 0
        for (data in userData!!) {
            if (subTypeParent != null) {
                if (data.serviceId == service.id && data.subtype == mActivity)
                    count++
            }
            else {
                if (data.serviceId == service.id && data.type == mActivity)
                    count++
            }
        }
        return count
    }

    private fun countServiceForEachType(mActivity: String, service: Service): Int {
        var count = 0
        for (userData in userData!!) {
            if (userData.serviceId == service.id && userData.type == mActivity) count += 1
        }
        return count
    }

    companion object {
        const val PARAM_SERVICE = "service"
        const val PARAM_DATA = "data"
        const val PARAM_IS_DATA_CENTRIC = "is_data_centric"
        const val PARAM_SUB_TYPE_PARENT = "sub_type_parent"
    }

}
