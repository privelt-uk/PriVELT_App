package com.kent.university.privelt.model.tiktok

import com.google.gson.annotations.SerializedName
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListDevice
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class TikTok(
    @SerializedName("Activity")
    val activity: TikTokActivity?,
    @SerializedName("App Settings")
    val appSettings: TikTokAppSettings?,
    @SerializedName("Profile")
    val profile: TikTokProfile?,
) {

    companion object {
        public var mUserParentId: String = ""
        public var mDeviceParentId: String = ""
        public var mInitialLocationDataId: String = ""
    }

    fun toUserData(serviceId: Long, date: Long, userParentId: String, deviceParentId: String, initialLocationDataId: String): List<UserData> {
        mUserParentId = userParentId
        mDeviceParentId = deviceParentId
        mInitialLocationDataId = initialLocationDataId

        val activityData = activity?.toUserData(serviceId, date, userParentId) ?: emptyList()
        val appSettingsData = appSettings?.toUserData(serviceId, date, userParentId) ?: emptyList()
        val profileData = profile?.toUserData(serviceId, date, userParentId) ?: emptyList()

        return activityData + appSettingsData + profileData
    }
}

data class TikTokActivity(
    @SerializedName("Login History")
    val loginHistory: TikTokLoginHistory?,
    @SerializedName("Most Recent Location Data")
    val mostRecentLocationData: TikTokLocationData?,
) {
    fun toUserData(
        serviceId: Long,
        date: Long,
        parentId: String
    ): List<UserData> {
        val loginHistoryData = loginHistory?.toUserData(serviceId, date, parentId) ?: emptyList()
        val mostRecentLocationDataData =
            mostRecentLocationData?.toUserData(serviceId, date, parentId) ?: emptyList()
        return loginHistoryData + mostRecentLocationDataData
    }
}

data class TikTokLoginHistory(
    @SerializedName("LoginHistoryList")
    val loginHistoryList: List<TikTokLoginHistoryItem>?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        return loginHistoryList?.flatMap { it.toUserData(serviceId, date, parentId) } ?: emptyList()
    }
}

data class TikTokLoginHistoryItem(
    @SerializedName("ip")
    val ip: String?,
    @SerializedName("DeviceModel")
    val deviceModel: String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "TikTok History Item: $title",
                type = getType(field.name),
                subtype = getSubType(field.name),
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = getParentId(field.name))
        }
        return userDataList
    }
}

data class TikTokLocationData(
    @SerializedName("LocationData")
    val locationData: TikTokLocationDataItem?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        return locationData?.toUserData(serviceId, date, parentId) ?: emptyList()
    }
}

data class TikTokLocationDataItem(
    @SerializedName("date")
    val date: String?,
    @SerializedName("GpsData")
    val gpsData: String?,
    @SerializedName("LastRegion")
    val lastRegion: String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "TikTok Location Data Item: $title",
                type = DataTypeList.LOCATION.title,
                subtype = DataSubTypeListLocation.LOCATION.value,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = TikTok.mInitialLocationDataId)
        }
        return userDataList
    }
}


private fun getSubType(fieldName: String): String {
    return when {
        fieldName.contains("lastRegion") -> DataSubTypeListLocation.LOCATION.value
        fieldName.contains("gpsData") -> DataSubTypeListLocation.LOCATION.value
        fieldName.contains("date") -> DataSubTypeListLocation.LOCATION.value
        fieldName.contains("ip") -> DataSubTypeListDevice.IP.value
        fieldName.contains("deviceModel") -> DataSubTypeListDevice.DEVICE_TYPE.value
        else -> fieldName.replace("_", " ")
    }
}

private fun getType(fieldName: String): String {
    return when {
        fieldName.contains("lastRegion") -> DataTypeList.LOCATION.title
        fieldName.contains("gpsData") -> DataTypeList.LOCATION.title
        fieldName.contains("date") -> DataTypeList.LOCATION.title
        fieldName.contains("ip") -> DataTypeList.DEVICE.title
        fieldName.contains("deviceModel") -> DataTypeList.DEVICE.title
        else -> fieldName.replace("_", " ")
    }
}

private fun getParentId(fieldName: String): String {
    return when {
        fieldName.contains("lastRegion") -> TikTok.mUserParentId
        fieldName.contains("gpsData") -> TikTok.mUserParentId
        fieldName.contains("date") -> TikTok.mUserParentId
        fieldName.contains("ip") -> TikTok.mDeviceParentId
        fieldName.contains("deviceModel") -> TikTok.mDeviceParentId
        else -> fieldName.replace("_", " ")
    }
}
