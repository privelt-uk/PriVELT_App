package com.kent.university.privelt.service.snapchat

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import com.snap.loginkit.models.MeData

import kotlin.reflect.full.memberProperties

data class SnapChatData(
    val displayName: String? = null,
    val externalId: String? = null,
    val idToken: String? = null,
    val bitmojiDataURL: String? = null,
    val bitmojiDataId: String? = null) {

    companion object {
        fun createFromMeData(meData: MeData): SnapChatData {
            return SnapChatData(meData.displayName, meData.externalId, meData.idToken, meData.bitmojiData?.twoDAvatarUrl, meData.bitmojiData?.avatarId)
        }
    }

    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val initialUserData = UserData(title = "Snapchat Profile",
            type = DataTypeList.PROFILE.title,
            subtype = "Snapchat Profile",
            value = "",
            serviceId = serviceId,
            date = date)
        val allData = mutableListOf<UserData>()
        allData.add(initialUserData)

        val nameUserData = UserData(title = "Snapchat Profile Display Name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = displayName ?: "",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(nameUserData)

        val externalIdUserData = UserData(title = "Snapchat Profile External Id",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "External Id: ${externalId ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(externalIdUserData)

        val idTokenUserData = UserData(title = "Snapchat Profile Id Token",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Id Token: ${idToken ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(idTokenUserData)

        val bitmojiDataUrlUserData = UserData(title = "Snapchat Profile Bitmoji Data URL",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Bitmoji Data Url: ${bitmojiDataURL ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(bitmojiDataUrlUserData)

        val bitmojiDataIdUserData = UserData(title = "Snapchat Profile Bitmoji Data Id",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = "Bitmoji Data Id: ${bitmojiDataId ?: ""}",
            serviceId = serviceId,
            date = date,
            parentId = initialUserData.id)
        allData.add(bitmojiDataIdUserData)
        return allData
    }
}

