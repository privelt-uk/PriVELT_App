/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.dashboard.card.data_metrics

import androidx.recyclerview.widget.RecyclerView
import com.kent.university.privelt.PriVELTApplication
import com.kent.university.privelt.databinding.CellMetricsBinding
import com.kent.university.privelt.events.DetailedCardEvent
import com.kent.university.privelt.model.Card
import com.kent.university.privelt.model.CardItem
import com.kent.university.privelt.service.base.DataTypeList
import net.neferett.webviewsextractor.model.UserDataTypes
import org.greenrobot.eventbus.EventBus
import java.util.*

class DataMetricsViewHolder internal constructor(private val binding: CellMetricsBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(card: Card, cardItem: CardItem) {
        binding.valueMetrics.text = cardItem.number.toString()
        if ((card.isService)) {
            val userDataType = UserDataTypes.getUserDataType(cardItem.name.uppercase(Locale.ROOT))
            if (userDataType != null)
                binding.imageType.setImageResource(userDataType.res)
            else
                binding.imageType.setImageResource(DataTypeList.getResourceFromTitle(cardItem.name))
        } else {
            val priVELTApplication =
                binding.imageType.context.applicationContext as PriVELTApplication
            val res = priVELTApplication.serviceHelper!!.getResIdWithName(cardItem.name)
            res?.let {
                binding.imageType.setImageResource(res)
            }

        }
        itemView.setOnClickListener { EventBus.getDefault().post(DetailedCardEvent(card)) }
    }

}
