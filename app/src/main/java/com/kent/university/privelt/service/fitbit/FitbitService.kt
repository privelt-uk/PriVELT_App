package com.kent.university.privelt.service.fitbit

import android.content.Context
import android.util.Base64
import android.util.Log
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.view.isVisible
import com.google.firebase.crashlytics.ktx.crashlytics
import com.google.firebase.ktx.Firebase
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.ApiResponseException
import com.kent.university.privelt.service.base.APIServiceListener
import com.kent.university.privelt.service.base.DataTypeList
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FitbitService: Service("Fitbit", false, "", "", "") {
    lateinit var APIServiceListener: APIServiceListener
    private val redirectUrl = "privelt://fitbit"
    val clientId = "238H5Z"
    val clientSecret = "7e8bf264520012701dbc4f0e0e82328e"
    var systemTime: Long = System.currentTimeMillis()

    fun insertServiceAndFetchData(token: String, userId: String, context: Context) = CoroutineScope(Dispatchers.IO).launch {

        CoroutineScope(Dispatchers.Main).launch {
            APIServiceListener.loading()
        }

        systemTime = System.currentTimeMillis()
        val id = insertOrGetService(context)
        getProfile(token, userId, id, context)
    }

    fun connection(context: Context, webView: WebView) {
        openBrowser(webView, context)
    }

    private fun openBrowser(webView: WebView, context: Context) {
        webView.isVisible = true
        webView.settings.javaScriptEnabled = true
        val codeVerifier = "01234567890123456789012345678901234567890123456789"//getRandomString(64)
        Log.d("OAuth code verifier", codeVerifier)
        val base64 = Base64.encodeToString(codeVerifier.encodeToByteArray(), Base64.NO_WRAP).replace("==", "")
        val codeChallenge = "-4cf-Mzo_qg9-uq0F4QwWhRh4AjcAqNx7SbYVsdmyQM"//encodeHeader(base64)
        Log.d("OAuth code challenge", codeChallenge)
        val redirectScheme = "privelt"
        val scope = "weight%20location%20settings%20profile%20nutrition%20activity%20sleep%20heartrate%20social"
        val url = "https://www.fitbit.com/oauth2/authorize?response_type=code&client_id=$clientId&redirect_uri=$redirectUrl&scope=$scope&state=state&code_challenge=$codeChallenge&code_challenge_method=S256"
        webView.loadUrl(url)
        // Set Redirect Listener
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                request?.let {
                    // Check if this url is our OAuth redirectUrl, otherwise ignore it
                    if (request.url.toString().startsWith(redirectScheme)) {
                        // This is our request. Parse the redirect URL query parameters to get the code
                        request.url.getQueryParameter("code")?.let { code ->
                            // Got code!
                            Log.d("OAuth code", code)
                            getToken(code, codeVerifier, context)
                        } ?: run {
                            // User cancelled the login flow
                            Log.d("OAuth", "Authorization code not received :(")
                            APIServiceListener.error("Authorization code not received :(")

                            val exception = ApiResponseException(
                                "Authorization code not received :(",
                                "",
                                500)
                            Firebase.crashlytics.recordException(exception)
                        }
                        return true
                    }
                }
                return false
            }
        }
    }

    fun getToken(code: String, codeVerifier: String, context: Context) {
        val retFitbit = RetrofitFitbitInstance.getApiRetrofitInstance()
        retFitbit.requestToken( "authorization_code", clientId, code, redirectUrl, codeVerifier)
            .enqueue(object: Callback<AuthRequestTokenResponse> {
                override fun onFailure(call: Call<AuthRequestTokenResponse>, t: Throwable) {
                    Log.e("fitbit", t.message.toString())
                    APIServiceListener.error(t.message.toString())
                    Firebase.crashlytics.recordException(t)
                }
                override fun onResponse(call: Call<AuthRequestTokenResponse>, response: Response<AuthRequestTokenResponse>) {
                    if (response.code() == 200) {
                        Log.d("fitbit","Generated token!")
                        response.body()?.let { responseBody ->
                            Log.d("Access token", responseBody.access_token)
                            val token = responseBody.access_token
                            val bearerToken = "Bearer $token"
                            insertServiceAndFetchData(bearerToken, responseBody.user_id, context)
                        }
                    } else {
                        Log.d("fitbit","Token exchange failed!")
                        Log.d("fitbit",response.body().toString())
                        val exception = ApiResponseException(
                            "Fetching token exchange failed!",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        APIServiceListener.error("Token exchange failed!")
                    }
                }
            })
    }

    private fun getProfile(token: String, userId: String, serviceId: Long, context: Context) {
        val userDataRepository = PriVELTDatabase.getInstance(context)?.userDataDao()

        val retFitbit = RetrofitFitbitInstance.getApiRetrofitInstance()
        retFitbit.requestProfile(userId, token)
            .enqueue(object: Callback<FitbitUser> {
                override fun onFailure(call: Call<FitbitUser>, t: Throwable) {
                    Log.e("fitbit", t.message.toString())
                    Firebase.crashlytics.recordException(t)
                    APIServiceListener.error(t.message.toString())
                }
                override fun onResponse(call: Call<FitbitUser>, response: Response<FitbitUser>) {
                    if (response.code() == 200) {
                        Log.d("fitbit","Fetched profile!")
                        response.body()?.user.let { it ->
                            if (it != null) {

                                val allData = mutableListOf<UserData>()
                                val test = response.body()?.user?.toUserData(serviceId, systemTime) ?: emptyList()
                                allData.addAll(test)
                                //save username
                                CoroutineScope(Dispatchers.IO).launch {
                                    val serviceDataRepository =
                                        PriVELTDatabase.getInstance(context)?.serviceDao()
                                    val service =
                                        serviceDataRepository?.getServiceWithName(this@FitbitService.name)
                                    service?.user = response.body()?.user?.displayName ?: ""
                                    serviceDataRepository?.updateServices(service)
                                }

                                CoroutineScope(Dispatchers.IO).launch {
                                    allData.forEach {
                                        if (it.isValid())
                                            userDataRepository?.insertUserData(it)
                                    }
                                }
                            }
                            APIServiceListener.success()
                        }
                    }
                    else{
                        Log.d("fitbit", response.message())
                        Log.d("fitbit","Fetching profile failed!")
                        val exception = ApiResponseException(
                            "Fetching profile failed!",
                            response.message(),
                            response.code())
                        Firebase.crashlytics.recordException(exception)
                        APIServiceListener.error("Fetching profile failed!")
                    }
                }
            })
    }
}