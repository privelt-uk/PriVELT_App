package com.kent.university.privelt.service.booking

import android.content.Context
import com.kent.university.privelt.model.Booking.BookingProfile
import com.kent.university.privelt.model.Booking.BookingReservationData
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataTypeList
import com.kent.university.privelt.service.base.SARService
import com.kent.university.privelt.service.base.State
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.jsoup.Jsoup
import java.io.InputStream

class BookingService : SARService() {

    override var sarRequestURL: String = "https://www.booking.com/content/dsar.html"
    override val service = Service("Booking", false, "", "", "")

    override fun parseSAR(context: Context, inputStream: InputStream) =
        CoroutineScope(Dispatchers.IO).launch {
            _stateFlow.emit(State.Loading)
            val html = inputStream.bufferedReader().use { it.readText() }

            val doc = Jsoup.parse(html)
            val tables = doc.select("table")
            val unixTime = System.currentTimeMillis()
            if (tables.size >= 2) {
                val table = doc.select("table")[2]
                var reservations: MutableList<BookingReservationData> = mutableListOf<BookingReservationData>()
                val allData = mutableListOf<UserData>()
                table.select("tr").drop(1).forEach {
                    val row = it.select("td")
                    val userData = UserData(title = "Booking Reservation",
                        type = DataTypeList.ACTIVITY.title,
                        subtype = "Booking Reservation",
                        value = "",
                        serviceId = service.id,
                        date = unixTime)
                    allData.add(userData)
                    if (row.size >= 26) {
                        val reservation = BookingReservationData(
                            row[2].text(),
                            row[4].text(),
                            row[5].text(),
                            row[6].text(),
                            row[7].text(),
                            row[8].text(),
                            row[9].text(),
                            row[10].text(),
                            row[11].text(),
                            row[12].text(),
                            row[13].text(),
                            row[14].text(),
                            row[18].text(),
                            row[19].text(),
                            row[20].text(),
                            row[23].text()
                        )
                        reservations.add(reservation)
                        allData.addAll(reservation.toUserData(0L, unixTime, userData.id))
                    }
                }
                extractProfile(context, html, unixTime, allData)
            } else {
                _stateFlow.emit(State.Error("The selected file is not a SAR file for this service"))
            }
        }
    private fun extractProfile(context: Context, html: String, date: Long, userData: List<UserData>) =
        CoroutineScope(Dispatchers.IO).launch {
            val firstName = regexFind(FIRST_NAME_REGEX, html).replace("</b>", "").replace("<", "")
            val lastName = regexFind(LAST_NAME_REGEX, html).replace("</b>", "").replace("<", "")
            val email = regexFind(EMAIL_REGEX, html).replace("</b>", "").replace("<", "")
            val address = regexFind(ADDRESS_REGEX, html).replace("</b>", "").replace("<", "")
            val phone = regexFind(PHONE_REGEX, html).replace("</b>", "").replace("<", "")
            val gender = regexFind(GENDER_REGEX, html).replace("</b>", "").replace("<", "")
            val currencyCode = regexFind(CURRENCY_CODE_REGEX, html).replace("</b>", "").replace("<", "")
            val language = regexFind(LANGUAGE_REGEX, html).replace("</b>", "").replace("<", "")
            val title = regexFind(TITLE_REGEX, html).replace("</b>", "").replace("<", "")
            val smokePreference = regexFind(SMOKE_PREFERENCE_REGEX, html).replace("</b>", "").replace("<", "")
            val companyName = regexFind(COMPANY_NAME_REGEX, html).replace("</b>", "").replace("<", "")
            val ageGroup = regexFind(AGE_GROUP_REGEX, html).replace("</b>", "").replace("<", "")
            val countryCode = regexFind(COUNTRY_CODE_REGEX, html).replace("</b>", "").replace("<", "")
            val profile = BookingProfile(currencyCode,
                language,
                title,
                address,
                gender,
                firstName,
                lastName,
                email,
                phone,
                smokePreference,
                companyName,ageGroup,
                countryCode)
            val allData = mutableListOf<UserData>()

            val userDataList = profile.toUserData(0L, date)

            allData.addAll(userDataList)
            saveUserDataToRepository(context, allData + userData, true)
        }
    private fun regexFind(regex: String, html: String): String {
        val values = Regex(regex, RegexOption.MULTILINE).find(html)?.groupValues
        return values?.first() ?: "Not found"
    }

    companion object {
        private const val FIRST_NAME_REGEX = "First name:</b> (.*?)<"
        private const val LAST_NAME_REGEX = "Last name:</b> (.*?)<"
        private const val EMAIL_REGEX = "Email:</b> (.*?)<"
        private const val ADDRESS_REGEX = "Address:</b> (.*?)<"
        private const val PHONE_REGEX = "Phone:</b> (.*?)<"
        private const val GENDER_REGEX = "Gender:</b> (.*?)<"
        private const val CURRENCY_CODE_REGEX = "Currency code:</b> (.*?)<"
        private const val LANGUAGE_REGEX = "Language:</b> (.*?)<"
        private const val TITLE_REGEX = "Title:</b> (.*?)<"
        private const val SMOKE_PREFERENCE_REGEX = "Smoke preference:</b> (.*?)<"
        private const val COMPANY_NAME_REGEX = "Company name:</b> (.*?)<"
        private const val AGE_GROUP_REGEX = "Age group:</b> (.*?)<"
        private const val COUNTRY_CODE_REGEX = "Country code:</b> (.*?)<"
    }

}
