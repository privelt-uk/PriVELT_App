package com.kent.university.privelt.model.snapchat

import com.google.gson.annotations.SerializedName
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataTypeList
import kotlin.reflect.full.declaredMemberProperties

data class SnapchatTerms(
    @SerializedName("Snap Inc. Terms of Service")
    val snapIncTerms: List<SnapIncTerms>?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val userDataList = snapIncTerms?.flatMap { it.toUserData(serviceId, date, parentId) }
        return userDataList ?: emptyList<UserData>()
    }
}

data class SnapIncTerms(
    @SerializedName("Version")
    val version: String?,
    @SerializedName("Acceptance Date")
    val acceptanceDate: String?
) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Snapchat Terms $title",
                type = DataTypeList.FEED.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}