/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.model

import android.content.Context
import com.kent.university.privelt.R
import com.kent.university.privelt.utils.sensors.SensorManager
import java.util.*

enum class Sensor(var title: String, val resId: Int, val isSensor: Boolean, val color: Int, val isDangerous: Boolean) {
    LOCATION("Location", R.drawable.ic_location, true, R.color.material_red, true),
    BLUETOOTH("Bluetooth", R.drawable.ic_bluetooth, true, R.color.material_pink, false),
    STORAGE("Storage", R.drawable.ic_storage, false, R.color.material_purple, true),
    WIFI("Wi-Fi", R.drawable.ic_wifi, true, R.color.material_indigo, false),
    NFC("NFC", R.drawable.ic_nfc, true, R.color.material_dark_grey, false),
    CONTACTS("Contacts", R.drawable.ic_contacts, false, R.color.material_blue, true),
    CALENDAR("Calendar", R.drawable.ic_calendar, false, R.color.material_cyan, true),
    SMS("SMS", R.drawable.ic_sms, false, R.color.material_teal, true),
    RECORD_AUDIO("Micro", R.drawable.ic_micro, false, R.color.material_green, true),
    GET_ACCOUNTS("Accounts", R.drawable.ic_contacts, false, R.color.material_lime, true),
    CAMERA("Camera", R.drawable.ic_camera, false, R.color.material_yellow, true),
    READ_PHONE("Phone State", R.drawable.ic_phone, false, R.color.material_orange, true),
    MEDIA_CONTENT_CONTROL("Playing Content", R.drawable.ic_media, false, R.color.material_brown, false),
    ACTIVITY_RECOGNITION("Activity Recognition", R.drawable.ic_sport, false, R.color.material_grey, true),
    NETWORK_STATE("Network State", R.drawable.ic_wifi, false, R.color.material_blue_grey, false),
    BODY_SENSORS("Body Sensors", R.drawable.ic_heart, false, R.color.material_black, true);

    private val applications: MutableList<Application>

    fun getApplications(): MutableList<Application> {
        return applications
    }

    fun addApplication(application: Application) {
        applications.add(application)
    }

    fun isEnabled(context: Context?): Boolean {
        return SensorManager.isEnabled(this, context!!)
    }

    init {
        applications = ArrayList()
    }
}