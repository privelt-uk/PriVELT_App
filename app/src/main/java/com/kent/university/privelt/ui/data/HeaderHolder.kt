package com.kent.university.privelt.ui.data

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import com.kent.university.privelt.R
import com.kent.university.privelt.model.UserData
import com.unnamed.b.atv.model.TreeNode
import com.unnamed.b.atv.model.TreeNode.BaseNodeViewHolder

class HeaderHolder(context: Context?, val allTheSame: Boolean, val index: Int) :
    BaseNodeViewHolder<UserData>(context) {
    private var arrowView: ImageView? = null
    override fun createNodeView(node: TreeNode, value: UserData): View {
        val inflater = LayoutInflater.from(context)
        val view: View = inflater.inflate(R.layout.layout_header_node, null, false)
        val tvValue = view.findViewById<View>(R.id.node_value) as TextView

        if (allTheSame)
            tvValue.text = value.title + " " + (index + 1)
        else
            tvValue.text = value.title

        //val iconView: PrintView = view.findViewById<View>(R.id.icon) as PrintView
        //iconView.setIconText(context.resources.getString(value.icon))
        arrowView = view.findViewById(R.id.arrow_icon)
        arrowView?.setImageResource(R.drawable.ic_arrow_right)
        if (node.isLeaf) {
            arrowView?.isVisible = false
        }
        return view
    }

    override fun toggle(active: Boolean) {
        arrowView?.rotation = if (active) 90f else 0f
    }
}