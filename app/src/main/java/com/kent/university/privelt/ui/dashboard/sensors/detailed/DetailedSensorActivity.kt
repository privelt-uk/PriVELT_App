/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.dashboard.sensors.detailed

import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kent.university.privelt.R
import com.kent.university.privelt.base.BaseActivity
import com.kent.university.privelt.databinding.ActivityDetailedSensorBinding
import com.kent.university.privelt.model.Sensor
import com.kent.university.privelt.ui.dashboard.sensors.SensorFragment
import com.kent.university.privelt.ui.dashboard.sensors.chart.sensor.SensorChartFragment
import com.kent.university.privelt.ui.dashboard.sensors.chart.sensor.SensorPagerActivity
import com.kent.university.privelt.utils.getSerializableBundle
import com.kent.university.privelt.utils.sentence.SentenceAdapter

class DetailedSensorActivity : BaseActivity() {

    private lateinit var binding: ActivityDetailedSensorBinding

    private lateinit var sensor: Sensor

    override val activityLayout: Int
        get() = R.layout.activity_detailed_sensor

    override fun configureViewModel() {}

    override fun configureDesign(savedInstanceState: Bundle?) {
        binding = ActivityDetailedSensorBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (savedInstanceState != null) {
            sensor = savedInstanceState.getSerializableBundle(
                SensorFragment.PARAM_SENSOR,
                Sensor::class.java
            )
        } else if (intent.extras != null) {
            sensor = intent.extras!!.getSerializableBundle(
                SensorFragment.PARAM_SENSOR,
                Sensor::class.java
            )
        }
        binding.logo.setImageResource(sensor.resId)
        binding.titleTV.text = sensor.title
        setUpRecyclerView()

        var riskValue = sensor.getApplications().size
        if (riskValue > 100) riskValue = 100
        when {
            riskValue == 0 -> binding.privacyValue.text =
                SentenceAdapter.adapt(resources.getString(R.string.global_privacy_value), "None")
            riskValue < 20 -> binding.privacyValue.text =
                SentenceAdapter.adapt(resources.getString(R.string.global_privacy_value), "Low")
            riskValue < 60 -> binding.privacyValue.text =
                SentenceAdapter.adapt(resources.getString(R.string.global_privacy_value), "Medium")
            else -> binding.privacyValue.text =
                SentenceAdapter.adapt(resources.getString(R.string.global_privacy_value), "High")
        }
        binding.riskProgress.progress = riskValue
    }

    private fun setUpRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        binding.applications.layoutManager = layoutManager
        val applicationsAdapter = DetailedSensorAdapter(sensor.getApplications())
        binding.applications.adapter = applicationsAdapter
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(SensorFragment.PARAM_SENSOR, sensor)
    }

}
