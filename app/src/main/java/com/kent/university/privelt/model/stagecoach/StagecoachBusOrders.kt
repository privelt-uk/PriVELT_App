package com.kent.university.privelt.model.stagecoach

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListActivity
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList

data class StagecoachBusOrders(
    val email: String,
    val id: String,
    val orderNumber: String,
    val channel: String,
    val ticketActive: String,
    val startDate: String,
    val endDate: String,
    val mobileActivationDate: String,
    val rttName: String,
    val description: String,
    val customerFirstName: String,
    val customerLastName: String,
    val billingAddress: String,
    val deliveryAddress: String) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val allData = mutableListOf<UserData>()

        val busOrderData = UserData(title = "Stagecoach Bus Order",
            type = DataTypeList.ACTIVITY.title,
            subtype = "Stagecoach Bus Order",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(busOrderData)

        val emailEntry = UserData(title = "Stagecoach Bus Order Data customer user email",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListProfile.EMAIL.value,
            value = email,
            serviceId = serviceId,
            date = date,
            parentId = busOrderData.id)
        allData.add(emailEntry)

        val idEntry = UserData(title = "Stagecoach Bus Order Data customer user id",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "user id: $id",
            serviceId = serviceId,
            date = date,
            parentId = busOrderData.id)
        allData.add(idEntry)

        val uuidEntry = UserData(title = "Stagecoach Bus Order Data user order number",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.BOOKING.value,
            value = "order number: $orderNumber",
            serviceId = serviceId,
            date = date,
            parentId = busOrderData.id)
        allData.add(uuidEntry)

        val titleEntry = UserData(title = "Stagecoach Bus Order Data user channel",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.BOOKING.value,
            value = "Chanel used to buy the ticket: $channel",
            serviceId = serviceId,
            date = date,
            parentId = busOrderData.id)
        allData.add(titleEntry)

        val startDateEntry = UserData(title = "Stagecoach Bus Order Data user start date",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.BOOKING.value,
            value = "start date: $startDate",
            serviceId = serviceId,
            date = date,
            parentId = busOrderData.id)
        allData.add(startDateEntry)

        val endDateEntry = UserData(title = "Stagecoach Bus Order Data user end date",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.BOOKING.value,
            value = "end date: $endDate",
            serviceId = serviceId,
            date = date,
            parentId = busOrderData.id)
        allData.add(endDateEntry)

        val ticketActiveEntry = UserData(title = "Stagecoach Bus Order Data ticket active",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.BOOKING.value,
            value = "ticket active: $ticketActive",
            serviceId = serviceId,
            date = date,
            parentId = busOrderData.id)
        allData.add(ticketActiveEntry)

        val mobileActivationDateEntry = UserData(title = "Stagecoach Bus Order Data ticket mobile activation date",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.BOOKING.value,
            value = "mobile activation date: $mobileActivationDate",
            serviceId = serviceId,
            date = date,
            parentId = busOrderData.id)
        allData.add(mobileActivationDateEntry)

        val rttNameEntry = UserData(title = "Stagecoach Bus Order Data ticket rtt name",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.BOOKING.value,
            value = "rtt name: $rttName",
            serviceId = serviceId,
            date = date,
            parentId = busOrderData.id)
        allData.add(rttNameEntry)

        val descriptionEntry = UserData(title = "Stagecoach Bus Order Data ticket description",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListActivity.BOOKING.value,
            value = "description: $description",
            serviceId = serviceId,
            date = date,
            parentId = busOrderData.id)
        allData.add(descriptionEntry)

        val firstNameEntry = UserData(title = "Stagecoach Bus Order Data customer first name",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = customerFirstName,
            serviceId = serviceId,
            date = date,
            parentId = busOrderData.id)
        allData.add(firstNameEntry)

        val lastNameEntry = UserData(title = "Stagecoach Bus Order Data customer last name",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = customerLastName,
            serviceId = serviceId,
            date = date,
            parentId = busOrderData.id)
        allData.add(lastNameEntry)

        val billingAddressEntry = UserData(title = "Stagecoach Bus Order Data billing address",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListProfile.ADDRESS.value,
            value = billingAddress,
            serviceId = serviceId,
            date = date,
            parentId = busOrderData.id)
        allData.add(billingAddressEntry)

        val phoneNumberEntry = UserData(title = "Stagecoach Bus Order Data delivery address",
            type = DataTypeList.ACTIVITY.title,
            subtype = DataSubTypeListProfile.ADDRESS.value,
            value = deliveryAddress,
            serviceId = serviceId,
            date = date,
            parentId = busOrderData.id)
        allData.add(phoneNumberEntry)
        return allData
    }
}
