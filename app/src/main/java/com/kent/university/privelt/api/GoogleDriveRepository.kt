package com.kent.university.privelt.api

import android.content.Context
import android.util.Log
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.json.gson.GsonFactory
import com.google.api.services.drive.Drive
import com.google.api.services.drive.DriveScopes
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.gson.Gson
import com.kent.university.privelt.PriVELTApplication
import com.kent.university.privelt.R
import com.kent.university.privelt.api.model.DatabaseDump
import com.kent.university.privelt.database.PriVELTDatabase
import com.kent.university.privelt.model.Settings
import com.kent.university.privelt.utils.DriveServiceHelper
import java.io.BufferedOutputStream
import java.io.BufferedReader
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream


object GoogleDriveRepository {

    const val FILE_NAME = "user_data.zip"

    fun saveDatabaseAsJsonToGoogleDrive(applicationContext: Context) {
        val serviceDataRepository = PriVELTDatabase.getInstance(applicationContext)?.serviceDao()
        val userDataRepository = PriVELTDatabase.getInstance(applicationContext)?.userDataDao()
        val settingsDataRepository = PriVELTDatabase.getInstance(applicationContext)?.settingsDao()
        val historyRepository = PriVELTDatabase.getInstance(applicationContext)?.historyPermissionDao()

        val historyPermissionList = historyRepository?.allHistoryPermission
        val userData = userDataRepository?.syncUserData
        val serviceList = serviceDataRepository?.allServices
        serviceList?.map { it.password = "" }

        val databaseDump = DatabaseDump(serviceList, userData, historyPermissionList)

        val gson = Gson()
        val json = gson.toJson(databaseDump)
        writeToFile(json, applicationContext)

        val fileSaved = applicationContext.filesDir.listFiles()?.find { it.name.contains(FILE_NAME) }!!

        val settings = settingsDataRepository?.instantSettings ?: Settings(false, null, null)
        val fileId = saveToGoogleDrive(applicationContext, fileSaved)
        Log.d("PriVELT saveDatabaseAsJsonToGoogleDrive", "file id $fileId")
        settings.googleDriveFileID2 = fileId
        settingsDataRepository?.updateSettings(settings)
    }

    private fun saveToGoogleDrive(
        applicationContext: Context,
        file: File
    ): String {

        val account = GoogleSignIn.getLastSignedInAccount(applicationContext)
        val credential = GoogleAccountCredential.usingOAuth2(
            PriVELTApplication.instance,
            listOf(DriveScopes.DRIVE_FILE)
        )
        credential.selectedAccount = account!!.account

        val googleDriveService =
            Drive.Builder(AndroidHttp.newCompatibleTransport(), GsonFactory(), credential)
                .setApplicationName(PriVELTApplication.instance?.resources?.getString(R.string.app_name))
                .build()

        val mDriveServiceHelper = DriveServiceHelper(googleDriveService)
        return mDriveServiceHelper.uploadFile(
            file
        )
    }

    private fun decompressString(data: ByteArray, fileName: String): String? {
        val inputStream = ByteArrayInputStream(data)
        val zip = ZipInputStream(inputStream)
        var entry: ZipEntry? = zip.nextEntry

        val outputStream = ByteArrayOutputStream()
        val buffer = ByteArray(1024)
        var bytesRead: Int

        while (entry != null) {
            if (entry.name == fileName) {
                while (zip.read(buffer).also { bytesRead = it } != -1) {
                    outputStream.write(buffer, 0, bytesRead)
                }
                zip.closeEntry()
                return outputStream.toString("UTF-8")
            }
            zip.closeEntry()
            entry = zip.nextEntry
        }

        return null
    }

    private fun readToFile(context: Context): String? {
        try {
            val inputStream = context.openFileInput(FILE_NAME)
            val inputStreamData = inputStream.readBytes()
            inputStream.close()
            val decompressedData = decompressString(inputStreamData, "user_data.json") // Use "user_data.json" as the file name to decompress
            return decompressedData
        } catch (e: IOException) {
            FirebaseCrashlytics.getInstance().recordException(e)
            Log.e("Exception", "File read failed: $e")
        }
        return null
    }

    private fun writeToFile(data: String, context: Context) {
        try {
            val userDataFile = File(context.filesDir, "user_data.json")
            userDataFile.writeText(data)

            // Create a temporary zip file containing "user_data.json"
            val tempZipFile = File(context.cacheDir, "temp_user_data.zip")
            val zip = ZipOutputStream(BufferedOutputStream(FileOutputStream(tempZipFile)))
            val zipEntry = ZipEntry("user_data.json")
            zip.putNextEntry(zipEntry)
            userDataFile.inputStream().copyTo(zip)
            zip.closeEntry()
            zip.close()

            // Read the content of the temporary zip file
            val compressedData = tempZipFile.readBytes()

            // Write the content to "user_data.zip"
            val outputStream = context.openFileOutput(FILE_NAME, Context.MODE_PRIVATE)
            outputStream.write(compressedData)
            outputStream.close()

            // Delete the temporary zip file
            tempZipFile.delete()
        } catch (e: IOException) {
            FirebaseCrashlytics.getInstance().recordException(e)
            Log.e("Exception", "File write failed: $e")
        }
    }

    fun saveToDatabase(applicationContext: Context) {
        val serviceDataRepository = PriVELTDatabase.getInstance(applicationContext)?.serviceDao()
        val userDataRepository = PriVELTDatabase.getInstance(applicationContext)?.userDataDao()
        val historyRepository = PriVELTDatabase.getInstance(applicationContext)?.historyPermissionDao()

        val content = readToFile(applicationContext)

        val gson = Gson()
        val databaseDump = gson.fromJson(content, DatabaseDump::class.java)

        databaseDump?.let {
            it.service?.forEach {
                serviceDataRepository?.insertServices(it)
            }
            it.userData?.forEach {
                userDataRepository?.insertUserData(it)
            }
            it.historyPermissionList?.forEach {
                historyRepository?.insertHistoryPermissionStatus(it)
            }
        }
    }
}