package com.kent.university.privelt.service.tiktok

import android.content.Context
import com.google.gson.Gson
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.model.tiktok.TikTok
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import com.kent.university.privelt.service.base.SARService
import com.kent.university.privelt.service.base.State
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.InputStream
import java.util.zip.ZipInputStream

class TikTokSarService: SARService() {

    override val sarRequestURL: String = "https://support.tiktok.com/en/account-and-privacy/personalized-ads-and-data/requesting-your-data"
    override val service: Service = Service("TikTok SAR", false, "", "", "")

    override fun parseSAR(context: Context, inputStream: InputStream) =
        CoroutineScope(Dispatchers.IO).launch {
            _stateFlow.emit(State.Loading)
            val date = System.currentTimeMillis()
            val zipInputStream = ZipInputStream(inputStream)
            val pairs = zipInputStream.use { zipStream ->
                generateSequence { zipStream.nextEntry }
                    .filter { it.name.endsWith(".json") }
                    .filterNot { it.name.startsWith("__") }
                    .map {
                        Pair(it.name, zipStream.reader().readText())
                    }
                    .toList()
            }
            val gson = Gson()
            val serviceId = 0L
            val allData = mutableListOf<UserData>()
            val initialUserData = UserData(title = "TikTok user data",
                type = DataTypeList.PROFILE.title,
                subtype = "TikTok user data",
                value = "",
                serviceId = service.id,
                date = date)
            val initialDeviceData = UserData(title = "TikTok device data",
                type = DataTypeList.DEVICE.title,
                subtype = "TikTok device data",
                value = "",
                serviceId = service.id,
                date = date)
            val initialLocationData = UserData(title = "TikTok location data",
                type = DataTypeList.LOCATION.title,
                subtype = "TikTok location data",
                value = "",
                serviceId = service.id,
                date = date)
            allData.add(initialUserData)
            allData.add(initialDeviceData)
            allData.add(initialLocationData)
            pairs.map {
                if (it.first.contains("user_data.json")) {
                    val userData = gson.fromJson(it.second, TikTok::class.java).toUserData(serviceId, date, initialUserData.id, initialDeviceData.id, initialLocationData.id)
                    allData.addAll(userData)
                }
            }

            saveUserDataToRepository(context, allData, true)
        }
}