package com.kent.university.privelt.service.uber

import com.google.api.client.util.DateTime
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ApiInterface {
    @Headers("Content-Type:application/json")
    @POST("oauth/v2/token")
    fun exchangeCodeForToken(@Query("client_secret") clientSecret: String,
                             @Query("client_id") clientId: String,
                             @Query("grant_type") grantType: String,
                             @Query("redirect_uri") redirectUri: String,
                             @Query("scope") scope: String,
                             @Query("code") code: String): Call<AuthTokenExchangeResponse>

    @GET("v1.2/me")
    fun profile(@Header("Authorization") bearerToken: String): Call<Profile>

    @Headers(
        "Content-Type:application/json", "Accept-Language: en_US"
    )
    @GET("v1.2/history")
    fun history(@Header("Authorization") bearerToken: String): Call<History>

    @Headers(
        "Content-Type:application/json", "Accept-Language: en_US"
    )
    @GET("v1.2/requests/{id}")
    fun requestDetails(@Header("Authorization") bearerToken: String,
                       @Path("id") id: String): Call<RequestItem>

}
class RetrofitInstance {
    companion object {
        private const val BASE_URL: String = "https://login.uber.com"
        private const val BASE_API_URL: String = "https://api.uber.com"

        fun getLoginRetrofitInstance(): ApiInterface {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiInterface::class.java)
        }

        fun getApiRetrofitInstance(): ApiInterface {
            return Retrofit.Builder()
                .baseUrl(BASE_API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiInterface::class.java)
        }
    }
}

data class AuthTokenExchangeResponse(val access_token: String,
                                     val token_type: String,
                                     val expires_in: Int,
                                     val refresh_token: String,
                                     val scope: String)

data class Profile(val picture: String,
                   val first_name: String,
                   val last_name: Int,
                   val uuid: String,
                   val rider_id: String,
                   val email: String,
                   val mobile_verified: Boolean,
                   val promo_code: String)

data class History(val count: Int,
                   val history: List<HistoryItem>)

data class HistoryItem(val status: String,
                       val distance: Float,
                       val product_id: String,
                       val start_time: DateTime,
                       val end_time: DateTime,
                       val request_id: String,
                       val request_time: DateTime,
                       val start_city: CityItem)

data class CityItem(val latitude: Float,
                    val longitude: Float,
                    val display_name: String)

data class RequestItem(val status: String,
                       val request_id: Float,
                       val product_id: String,
                       val shared: DateTime,
                       val surge_multiplier: String,
                       val driver: DriverItem)

data class DriverItem(val phone_number: String,
                      val sms_number: String,
                      val rating: Int,
                      val picture_url: String,
                      val name: String,
                      val vehicle: VehicleItem,
                      val location: LocationItem,
                      val pickup: PlaceItem,
                      val destination: PlaceItem,
                      val waypoints: List<WaypointsItem>,
                      val riders: List<RidersItem>)

data class VehicleItem(val make: String,
                       val model: String,
                       val license_plate: Int,
                       val picture_url: String)

data class LocationItem(val latitude: Float,
                        val longitude: Float,
                        val bearing: Int)

data class PlaceItem(val alias: String,
                     val latitude: Float,
                     val longitude: Float,
                     val name: String,
                     val address: String,
                     val eta: Int)

data class WaypointsItem(val rider_id: String?,
                         val latitude: Float,
                         val longitude: Float,
                         val type: String)

data class RidersItem(val rider_id: String?,
                      val first_name: String,
                      val me: Boolean)