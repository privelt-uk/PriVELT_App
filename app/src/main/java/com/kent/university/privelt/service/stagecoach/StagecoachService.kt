package com.kent.university.privelt.service.stagecoach

import android.content.Context
import com.facebook.FacebookSdk.getApplicationContext
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.stagecoach.StagecoachBusOrders
import com.kent.university.privelt.model.stagecoach.StagecoachProfile
import com.kent.university.privelt.service.base.SARService
import com.kent.university.privelt.service.base.State
import com.tom_roush.pdfbox.android.PDFBoxResourceLoader
import com.tom_roush.pdfbox.pdmodel.PDDocument
import com.tom_roush.pdfbox.text.PDFTextStripper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.InputStream
import java.util.zip.ZipInputStream

class StagecoachService : SARService() {

    override var sarRequestURL: String = "https://www.stagecoachbus.com/media-library/files/national/final_uk_bus_dsar.pdf"
    override val service = Service("Stagecoach", false, "", "", "")

    override fun parseSAR(context: Context, inputStream: InputStream) =
        CoroutineScope(Dispatchers.IO).launch {
            _stateFlow.emit(State.Loading)
            val date = System.currentTimeMillis()
            PDFBoxResourceLoader.init(getApplicationContext())
            val stripper = PDFTextStripper()
            val userData = ZipInputStream(inputStream).use { zipStream ->
                generateSequence { zipStream.nextEntry }
                    .filter { it.name.endsWith(".pdf") }
                    .filterNot { it.name.startsWith("__") }
                    .map {
                        if (it.name.contains("Account Details.pdf")) {
                            val document = PDDocument.load(zipStream)
                            val text = stripper.getText(document)
                            val lastPage = document.numberOfPages
                            stripper.startPage = lastPage
                            stripper.endPage = lastPage
                            document.close()
                            val allLines = text.split("\n").filterNot { it == " " || it == "" }.drop(1)
                            var items = allLines[0].split(" ")
                            items = items.dropLast(1)

                            val updateDateTime = items.last()
                            items = items.dropLast(1)
                            val updateDate = items.last()
                            items = items.dropLast(1)

                            val createdDateTime = items.last()
                            items = items.dropLast(1)
                            val createdDate = items.last()
                            items = items.dropLast(1)

                            val profile = StagecoachProfile(items[0], items[1], items[2], items[3], items[4], items[5], items[6], "$createdDate $createdDateTime", "$updateDate $updateDateTime")
                            profile.toUserData(0, date)
                        } else if (it.name.contains("Bus Orders.pdf")) {
                            val document = PDDocument.load(zipStream)
                            val lastPage = document.numberOfPages
                            stripper.startPage = 1
                            stripper.endPage = lastPage
                            val text = stripper.getText(document)
                            document.close()
                            val allLines = text.split("\n").filterNot { it == " " || it == "" }
                            val index = allLines.indexOf("o_customer_first_name o_customer_last_name o_billing_address o_delivery_address Null")
                            val arrays = allLines.chunked(index)
                            val combined = arrays.first()
                                .zip(arrays.last()) { a, b -> "$a $b"}
                                    .drop(1)
                                    .map { string -> string.replace(",,,,,", "-") }
                            val busOrders = combined.map { string ->
                                val split = string.split(" ")
                                StagecoachBusOrders(split[0], split[1],split[2], split[3],split[4], split[5],split[6], split[7],split[8], split[9], split[10], split[11], split[12], split[13])
                            }.flatMap { it.toUserData(0,date) }
                            busOrders
                        } else {
                            null
                        }
                    }
                    .filterNotNull()
                    .flatten()
                    .toList()
            }

            if (userData.isEmpty()) {
                _stateFlow.emit(State.Error("The selected file is not a SAR file for this service"))
                return@launch
            } else {
                saveUserDataToRepository(context, userData)
                _stateFlow.emit(State.Success)
            }
        }
}
