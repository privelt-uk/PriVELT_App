/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.dashboard.card

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import com.kent.university.privelt.databinding.DialogExtractionDateAlertBinding
import com.kent.university.privelt.model.Card
import java.text.SimpleDateFormat
import java.util.*

class ExtractionDateAlertDialog : DialogFragment() {

    companion object {
        fun newInstance(card: Card): ExtractionDateAlertDialog {
            val f = ExtractionDateAlertDialog()

            // Supply num input as an argument.

            // Supply num input as an argument.
            val args = Bundle()
            args.putSerializable("card", card)
            f.arguments = args
            return f
        }
    }

    var onDateChosen : ((Long) -> Unit)? = null
    lateinit var card: Card
    private lateinit var binding: DialogExtractionDateAlertBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout to use as dialog or embedded fragment
        binding = DialogExtractionDateAlertBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        card = arguments?.getSerializable("card") as Card
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        populateSpinner(card)
    }

    private fun populateSpinner(card: Card) {
        val spinnerArray: MutableList<String> = ArrayList()
        val dateChoices : MutableList<Long> = ArrayList()

        val dates =  card.metrics.map { it.date }.distinct()
        dates.forEach {
            val formatter = SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
            val dateString = formatter.format(Date(it))
            if (!spinnerArray.contains(dateString)) {
                spinnerArray.add(dateString)
                dateChoices.add(it)
            }
        }

        val adapter = ArrayAdapter(
            binding.root.context, android.R.layout.simple_spinner_item, spinnerArray
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.dateSpinner.adapter = adapter

        binding.confirm.setOnClickListener {
            val pos = binding.dateSpinner.selectedItemPosition
            val date = dates[pos]
            onDateChosen?.invoke(date)
            dismiss()
        }
    }
}