/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.api

import android.content.Context
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.service.base.ServiceController
import net.neferett.webviewsinjector.services.LoginService
import net.neferett.webviewsinjector.services.ServiceManager

class ServiceHelper(context: Context?) {

    private val serviceManager: ServiceManager = ServiceManager(context)
    private val serviceNames: ArrayList<String>
    private val serviceResIds: ArrayList<Int>

    fun getRemainingServices(subscribedServices: List<Service>): List<String> {
        val notSubscribed = ArrayList<String>()
        //remove all
        //notSubscribed.addAll(serviceNames)
        //Avoid not working services
        notSubscribed.remove("Strava(R)")
        notSubscribed.remove("Trainline(R)")
        notSubscribed.remove("Facebook(R)")
        notSubscribed.remove("Instagram(R)")
        notSubscribed.remove("Google(R)")
        notSubscribed.remove("Twitter(R)")
        notSubscribed.remove("Pinterest(R)")
        for (service in subscribedServices) notSubscribed.remove(service.name + ("(R)"))
        return notSubscribed.sorted()
    }

    fun getResIdWithName(name: String): Int? {
        for (i in 0 until serviceNames.size) if (serviceNames[i].contains(name)) return serviceResIds[i]
        for (service in ServiceController.serviceList) if (service.service.name.contains(name)) return service.icon
        return null
    }

    fun getServiceWithName(name: String): LoginService? {
        for (i in 0 until serviceNames.size) {
            if (serviceNames[i].contains(name)) {
                return serviceManager.serviceList[i]
            }
        }
        return null
    }

    init {
        serviceManager.loadServices()
        serviceNames = ArrayList()
        serviceResIds = ArrayList()
        for (loginService in serviceManager.serviceList) {
            serviceNames.add(loginService.name + "(R)")
            serviceResIds.add(loginService.drawableLogo)
        }
    }

}
