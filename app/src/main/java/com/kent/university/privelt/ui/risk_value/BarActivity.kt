/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.risk_value

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.kent.university.privelt.R
import com.kent.university.privelt.base.BaseActivity
import com.kent.university.privelt.databinding.ActivityBarchartBinding
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.utils.capitalize
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.ceil

class BarActivity : BaseActivity() {

    private lateinit var binding: ActivityBarchartBinding

    private var riskValueViewModel: RiskValueViewModel? = null
    private var services: List<Service>? = null
    private var userDatas: List<UserData>? = null
    private var type: String? = null
    private var service: String? = null
    private var isDataCentric = false

    override fun configureDesign(savedInstanceState: Bundle?) {
        binding = ActivityBarchartBinding.inflate(layoutInflater)
        setContentView(binding.root)
        services = ArrayList()
        userDatas = ArrayList()
        if (savedInstanceState != null) {
            type = savedInstanceState.getString(RiskValueActivity.PARAM_DATA)
            service = savedInstanceState.getString(RiskValueActivity.PARAM_SERVICE)
        } else if (intent != null) {
            type = intent.getStringExtra(RiskValueActivity.PARAM_DATA)
            service = intent.getStringExtra(RiskValueActivity.PARAM_SERVICE)
        }
        isDataCentric = !(type == null || type!!.isEmpty())
        title = if (isDataCentric) capitalize(type!!) else service
        binding.chart.description.isEnabled = false

        // enable touch gestures
        binding.chart.setTouchEnabled(true)

        binding.chart.dragDecelerationFrictionCoef = 0.9f

        // enable scaling and dragging
        binding.chart.isDragEnabled = true
        binding.chart.setScaleEnabled(false)
        binding.chart.setDrawGridBackground(false)
        binding.chart.isHighlightPerDragEnabled = false
        binding.chart.isHighlightFullBarEnabled = true

        binding.chart.xAxis.setDrawLimitLinesBehindData(true)

        // set an alternative background color
        binding.chart.setBackgroundColor(Color.WHITE)
        binding.chart.setViewPortOffsets(0f, 0f, 0f, 0f)

        val xAxis = binding.chart.xAxis
        xAxis.position = XAxis.XAxisPosition.TOP_INSIDE
        xAxis.textSize = 10f
        xAxis.setCenterAxisLabels(false)
        xAxis.textColor = Color.WHITE
        xAxis.setDrawAxisLine(false)
        xAxis.setDrawGridLines(false)
        xAxis.setDrawLabels(true)
        xAxis.textColor = Color.rgb(0, 0, 0)
        xAxis.granularity = 1f
        xAxis.isGranularityEnabled = true
        binding.chart.axisLeft.granularity = 1f
        val leftAxis = binding.chart.axisLeft
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
        leftAxis.textColor = Color.rgb(0, 0, 0)
        leftAxis.setDrawGridLines(true)
        leftAxis.isGranularityEnabled = true
        leftAxis.axisMinimum = 0f
        leftAxis.yOffset = -9f

        val rightAxis = binding.chart.axisRight
        rightAxis.isEnabled = false

        val legend: Legend = binding.chart.legend
        legend.isEnabled = false
        configureViewModel()
        getServices()
        userdatas
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(RiskValueActivity.PARAM_SERVICE, service)
        outState.putString(RiskValueActivity.PARAM_DATA, type)
    }

    override val activityLayout: Int
        get() = R.layout.activity_barchart

    override fun configureViewModel() {
        riskValueViewModel = getViewModel(RiskValueViewModel::class.java)
        riskValueViewModel?.init()
    }

    private fun updateUserDatas(userData: List<UserData>) {
        userDatas = userData
        if (userDatas!!.isNotEmpty() && services!!.isNotEmpty()) loadData()
    }

    private fun updateServices(services: List<Service>) {
        this.services = services
        if (userDatas!!.isNotEmpty() && services.isNotEmpty()) loadData()
    }

    //TODO: refactor les requêtes SQL
    private fun getServices() {
        riskValueViewModel!!.services?.observe(this) { services: List<Service> ->
            updateServices(
                services
            )
        }
    }

    private val userdatas: Unit
        get() {
            riskValueViewModel!!.userDatas?.observe(this) { userData: List<UserData> ->
                updateUserDatas(
                    userData
                )
            }
        }

    private val numberOfTypes: Array<String>
        get() {
            val types: MutableSet<String> = HashSet()
            for ((_, type1) in userDatas!!) {
                types.add(type1)
            }
            return types.toTypedArray()
        }

    private fun countTypeForEachService(mActivity: String, service: Service, date: Long): Float {
        var count = 0F
        for (userData in userDatas!!) {
            if (userData.serviceId == service.id && userData.type == mActivity && userData.date == date) count += 1
        }
        return count
    }

    private fun countServiceForEachType(mActivity: String, service: Service, date: Long): Float {
        var count = 0F
        for (userData in userDatas!!) {
            if (userData.serviceId == service.id && userData.type == mActivity && date == userData.date) count += 1
        }
        return count
    }

    @SuppressLint("SimpleDateFormat")
    private fun getDate(milliSeconds: Long): String? {
        // Create a DateFormatter object for displaying date in specified format.
        val formatter = SimpleDateFormat("dd/MM")

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = milliSeconds
        return formatter.format(calendar.time)
    }
    private fun arrayListToPrimitiveArrayPermission(array: List<Float>): FloatArray {
        val res = FloatArray(array.size)
        for ((index, value) in array.withIndex())
            res[index] = value
        return res
    }

    private fun getLimitLineAt(xIndex: Int): LimitLine {
        val ll = LimitLine(xIndex.toFloat()) // set where the line should be drawn
        ll.lineColor = Color.BLACK
        ll.enableDashedLine(10f, 10f, 0f)
        ll.lineWidth = 0.2f
        return ll
    }

    private fun loadData() {
        val values = ArrayList<BarEntry>()
        val mActivities = numberOfTypes
        var i = 0F
        val datesIndexes: ArrayList<String> = ArrayList()

        if (!isDataCentric) {
            val service = services?.find { it.name == this.service }!!
            val dates = userDatas?.filter { it.serviceId == service.id }?.map { it.date }?.distinct()!!
            for (date in dates) {
                val results = mActivities.map { countTypeForEachService(it, service, date) }
                values.add(BarEntry(i++, arrayListToPrimitiveArrayPermission(results)))
                datesIndexes.add(getDate(date)!!)
                binding.chart.xAxis?.addLimitLine(getLimitLineAt(i.toInt()))
            }
        } else {
            val dates = userDatas?.map { it.date }?.distinct()!!
            for (date in dates) {
                val results = services!!.map { countServiceForEachType(type!!, it, date) }
                values.add(BarEntry(i++, arrayListToPrimitiveArrayPermission(results)))
                datesIndexes.add(getDate(date)!!)
                binding.chart.xAxis?.addLimitLine(getLimitLineAt(i.toInt()))
            }
        }

        val set1 = BarDataSet(values, "Activated sensors")
        set1.setDrawIcons(false)


        val sets: MutableList<IBarDataSet> = ArrayList()
        sets.add(set1)
        /*for ((j, values) in allValues.withIndex()) {
            val set = BarDataSet(values, if (isDataCentric) services!![j].name else mActivities[j])
            Random()
            val color = getRandomColor()
            set.color = color
            sets.add(set)
        }*/
        val data = BarData(sets)
        data.setValueFormatter(LargeValueFormatter())
        binding.chart.data = data
        binding.chart.data?.setValueTextColor(Color.WHITE)
        binding.chart.data?.setDrawValues(true)
        binding.chart.data?.setValueTextSize(12f)
        binding.chart.setDrawValueAboveBar(false)
        binding.chart.data?.setValueFormatter(object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                if (value == 0f)
                    return ""
                return if (value.toDouble() != ceil(value.toDouble())) "Off"
                else value.toInt().toString()
            }
        })
        binding.chart.setFitBars(true)
        binding.chart.xAxis?.setLabelCount(7, false)
        binding.chart.moveViewToX(set1.entryCount.toFloat())
        binding.chart.xAxis?.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return if (value.toInt() < datesIndexes.size) datesIndexes[value.toInt()] else ""
            }
        }

        binding.chart.data?.isHighlightEnabled = false
        binding.chart.setVisibleXRange(7f, 7f)
        binding.chart.invalidate()
        if (!isDataCentric) {
            val service = services?.find { it.name == this.service }!!
            binding.heading.text = "Number of data extracted from ${service.name} over time"
        }
        else
            binding.heading.text = "Number of $type data extracted over time"
    }

}
