package com.kent.university.privelt.service.flickr

import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.service.base.DataSubTypeListLocation
import com.kent.university.privelt.service.base.DataSubTypeListProfile
import com.kent.university.privelt.service.base.DataTypeList
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import kotlin.reflect.full.declaredMemberProperties

interface ApiInterface {
    @GET("oauth/request_token")
    fun requestToken(
        @Query("oauth_nonce") authNonce: String,
        @Query("oauth_timestamp") authTimestamp: String,
        @Query("oauth_consumer_key") authConsumerKey: String,
        @Query("oauth_signature_method") authSignatureMethod: String,
        @Query("oauth_version") authVersion: String,
        @Query("oauth_signature") authSignature: String,
        @Query("oauth_callback") authCallback: String
        ): Call<String>

    @GET("oauth/access_token")
    fun accessToken(
        @Query("oauth_token") authToken: String,
        @Query("oauth_consumer_key") authConsumerKey: String,
        @Query("oauth_nonce") authNonce: String,
        @Query("oauth_timestamp") authTimestamp: String,
        @Query("oauth_signature") authSignature: String,
        @Query("oauth_signature_method") authSignatureMethod: String,
        @Query("oauth_verifier") authVerifier: String,
        @Query("oauth_version") authVersion: String
    ): Call<String>

    @GET("rest")
    fun getProfile(
        @Query("api_key") apiKey: String,
        @Query("method") method: String,
        @Query("user_id") userId: String,
        @Query("format") format: String,
        @Query("nojsoncallback") noJsonCallback: String
        ): Call<FlickrUser>

    @GET("rest")
    fun getContacts(
        @Query("format") format: String,
        @Query("method") method: String,
        @Query("nojsoncallback") noJsonCallback: String,
        @Query("oauth_consumer_key") authConsumerKey: String,
        @Query("oauth_nonce") authNonce: String,
        @Query("oauth_timestamp") authTimestamp: String,
        @Query("oauth_token") authToken: String,
        @Query("oauth_signature") authSignature: String,
        @Query("oauth_signature_method") authSignatureMethod: String,
        @Query("oauth_version") authVersion: String
    ): Call<FlickrContacts>

}

data class FlickrUser(val profile: FlickrUserDetails)

data class FlickrUserDetails(val join_date: String,
                             val occupation: String,
                             val hometown: String,
                             val showcase_set: String,
                             val showcase_set_title: String,
                             val first_name: String,
                             val last_name: String,
                             val profile_description: String,
                             val city: String,
                             val country: String,
                             val facebook: String,
                             val twitter: String,
                             val tumblr: String,
                             val instagram: String,
                             val pinterest: String) {
    fun toUserData(serviceId: Long, date: Long): List<UserData> {
        val allData = mutableListOf<UserData>()

        val profileUserData = UserData(title = "Flickr Profile",
            type = DataTypeList.PROFILE.title,
            subtype = "Flickr Profile",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(profileUserData)

        val locationUserData = UserData(title = "Flickr Locations",
            type = DataTypeList.LOCATION.title,
            subtype = "Flickr Locations",
            value = "",
            serviceId = serviceId,
            date = date)
        allData.add(locationUserData)

        val joinDateData = UserData(title = "Flickr Join Date",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = join_date,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(joinDateData)

        val occupationData = UserData(title = "Flickr Occupation",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.JOB_TITLE.value,
            value = occupation,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(occupationData)

        val hometownData = UserData(title = "Flickr Hometown",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = hometown,
            serviceId = serviceId,
            date = date,
            parentId = locationUserData.id)
        allData.add(hometownData)

        val showcaseSetData = UserData(title = "Flickr Showcase Set",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = showcase_set,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(showcaseSetData)

        val showcaseSetTitleData = UserData(title = "Flickr Showcase Set Title",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.PREFERENCES.value,
            value = showcase_set_title,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(showcaseSetTitleData)

        val firstNameData = UserData(title = "Flickr first Name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = first_name,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(firstNameData)

        val lastNameData = UserData(title = "Flickr Last Name",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.NAME.value,
            value = last_name,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(lastNameData)

        val profileDescriptionData = UserData(title = "Flickr Profile Description",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.OTHER.value,
            value = "Profile description: $profile_description",
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(profileDescriptionData)

        val cityData = UserData(title = "Flickr City",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = city,
            serviceId = serviceId,
            date = date,
            parentId = locationUserData.id)
        allData.add(cityData)

        val countryData = UserData(title = "Flickr Country",
            type = DataTypeList.LOCATION.title,
            subtype = DataSubTypeListLocation.LOCATION.value,
            value = country,
            serviceId = serviceId,
            date = date,
            parentId = locationUserData.id)
        allData.add(countryData)

        val facebookData = UserData(title = "Flickr Facebook",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.CONNECTED_ACCOUNTS.value,
            value = facebook,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(facebookData)

        val twitterData = UserData(title = "Flickr Twitter",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.CONNECTED_ACCOUNTS.value,
            value = twitter,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(twitterData)

        val tumblrData = UserData(title = "Flickr Tumblr",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.CONNECTED_ACCOUNTS.value,
            value = tumblr,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(tumblrData)

        val instagramData = UserData(title = "Flickr Instagram",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.CONNECTED_ACCOUNTS.value,
            value = instagram,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(instagramData)

        val pinterestData = UserData(title = "Flickr Pinterest",
            type = DataTypeList.PROFILE.title,
            subtype = DataSubTypeListProfile.CONNECTED_ACCOUNTS.value,
            value = pinterest,
            serviceId = serviceId,
            date = date,
            parentId = profileUserData.id)
        allData.add(pinterestData)
        return  allData
    }
}

data class FlickrContacts(val contacts: FlickrContactDetails)

data class FlickrContactDetails(val contact: List<FlickrContactItem>) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val allData = mutableListOf<UserData>()
        val contactsData = contact.map { it.toUserData(serviceId, date, parentId) }.flatten()
        allData.addAll(contactsData)
        return allData
    }
}

data class FlickrContactItem(val realname: String,
                             val friend: Int,
                             val family: Int,
                             val location: String?) {
    fun toUserData(serviceId: Long, date: Long, parentId: String): List<UserData> {
        val declaredFields = this.javaClass.kotlin.declaredMemberProperties
        val userDataList = declaredFields.map { field ->
            val title = field.name.replace("_", " ")
            UserData(title = "Flickr $title",
                type = DataTypeList.PROFILE.title,
                subtype = title,
                value = field.getter.call(this).toString(),
                serviceId = serviceId,
                date = date,
                parentId = parentId)
        }
        return userDataList
    }
}


class RetrofitFlickrInstance {
    companion object {
        val BASE_URL: String = "https://www.flickr.com/services/"
        val AUTH_URL: String = "https://www.flickr.com/services/oauth/authorize"

        fun getStringRetrofitInstance(): ApiInterface {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build()
                .create(ApiInterface::class.java)
        }

        fun getRetrofitInstance(): ApiInterface {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ApiInterface::class.java)
        }
    }
}