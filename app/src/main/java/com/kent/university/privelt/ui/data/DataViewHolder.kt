/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.data

import android.text.TextUtils
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.kent.university.privelt.R
import com.kent.university.privelt.databinding.CellUserdataBinding
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.utils.capitalize

class DataViewHolder internal constructor(private val binding: CellUserdataBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(userData: UserData, isFirstDepth: Boolean, areItemsTheSame: Boolean, position: Int) {
        binding.title.isVisible = isFirstDepth
        binding.title.text = capitalize(userData.type)
        if (areItemsTheSame)
            binding.title.text = "${capitalize(userData.type)} ${position + 1}"
        else
            binding.title.text = capitalize(userData.type)
        binding.value.text = capitalize(userData.subtype)
        binding.concatenatedData.text = TextUtils.join("\n", userData.unConcatenatedData)
        binding.concatenatedData.isVisible = userData.value.isNotEmpty()

        binding.icon.setImageResource(R.drawable.ic_arrow_right)
        binding.icon.isVisible = userData.hasChildren
    }

}
