/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package com.kent.university.privelt.ui.dashboard.card

import android.app.Dialog
import android.os.Bundle
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.kent.university.privelt.databinding.AlertImportDataBinding
import com.kent.university.privelt.databinding.AlertImportDataChooseBinding

class ChooseServiceTypeDialogFragment(
    private val listener: (Int) -> Unit,
) : DialogFragment() {


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {
            val builder = AlertDialog.Builder(it)
            val inflater = requireActivity().layoutInflater

            val binding = AlertImportDataChooseBinding.inflate(inflater)
            val view = binding.root

            binding.servicesList.setHasFixedSize(true)
            binding.servicesList.layoutManager = LinearLayoutManager(context)
            binding.servicesList.adapter = ChooseServiceTypeAdapter(listOf("Extraction based on API", "Extraction based on SAR"), listener)
            binding.help.setOnClickListener {
                val SNACKBAR_MAX_LINES = 8 // Increase maximum SnackBar line limit above 2
                val snackbar = Snackbar.make(binding.root, "An API service connects directly to the service provider and fetch user data in on go, while connecting SAR service requires two steps: requesting the user data from the service provider, and then import the received data files in the PriVELT app.", Snackbar.LENGTH_LONG)
                (snackbar.view.findViewById(com.google.android.material.R.id.snackbar_text) as TextView)
                    .run {
                        maxLines = SNACKBAR_MAX_LINES
                    }
                snackbar.show()
            }

            builder.setView(view)

            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }

}
