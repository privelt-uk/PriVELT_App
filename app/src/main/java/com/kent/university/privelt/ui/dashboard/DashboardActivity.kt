/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.dashboard

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.kent.university.privelt.R
import com.kent.university.privelt.base.BaseActivity
import com.kent.university.privelt.databinding.ActivityDashboardBinding
import com.kent.university.privelt.ui.dashboard.card.CardFragment
import com.kent.university.privelt.ui.dashboard.sensors.SensorFragment
import com.kent.university.privelt.ui.dashboard.user.UserFragment
import com.kent.university.privelt.ui.settings.SettingsFragment
import com.kent.university.privelt.utils.sensors.TemporarySaveHistoryPermission

class DashboardActivity : BaseActivity() {

    private var toolbar: ActionBar? = null
    var watchlist: MenuItem? = null

    override fun configureViewModel() {
        /* disabled for now if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                val alertDialog = AlertDialog.Builder(this)
                alertDialog.setTitle(R.string.data_extraction)
                    .setMessage(R.string.overlay_permission)
                    .setPositiveButton(R.string.allow) { _: DialogInterface?, _: Int ->
                        val myIntent = Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION)
                        this.startActivityForResult(myIntent, 101)
                    }.setNegativeButton(R.string.deny, null)
                alertDialog.show()
            }
        }*/
    }

    override fun configureDesign(savedInstanceState: Bundle?) {
        val binding = ActivityDashboardBinding.inflate(layoutInflater)
        setContentView(binding.root)
        toolbar = supportActionBar
        binding.navigationView.setOnNavigationItemSelectedListener(
            mOnNavigationItemSelectedListener
        )
        toolbar!!.title = "Dashboard"
        loadFragment(CardFragment())
        TemporarySaveHistoryPermission.load(applicationContext)
        /*val ex = Executors.newSingleThreadExecutor()
        ex.execute {
            val test = OntologyBuilder(PriVELTDatabase.getInstance(this)!!)
            test.build(this)
        }*/
    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            val fragment: Fragment
            when (item.itemId) {
                R.id.navigation_service -> {
                    fragment = CardFragment()
                    loadFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_sensors -> {
                    fragment = SensorFragment()
                    loadFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.navigation_settings -> {
                    fragment = SettingsFragment()
                    loadFragment(fragment)
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    private fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        val settings = menu.findItem(R.id.settings)
        val risk = menu.findItem(R.id.risk)
        val name = menu.findItem(R.id.name)
        watchlist = menu.findItem(R.id.watchlist)
        val date = menu.findItem(R.id.date)
        val status = menu.findItem(R.id.status)

        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        settings.isVisible = false
        risk.isVisible = fragment !is SettingsFragment
        name.isVisible = fragment !is SettingsFragment
        status.isVisible = fragment is SensorFragment
        watchlist!!.isVisible = fragment is CardFragment
        date.isVisible = fragment is CardFragment
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.dashboard_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.settings) {
            startActivityForResult(Intent(this, SettingsFragment::class.java), REQUEST_SETTINGS)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, resultData: Intent?) {
        if (requestCode == REQUEST_SETTINGS && resultCode == Activity.RESULT_OK) {
            finish()
        }
        if (requestCode == 101) {
            if (Settings.canDrawOverlays(this)) {
                Toast.makeText(
                    this,
                    "Display over app permission was successfully granted",
                    Toast.LENGTH_LONG
                ).show()
            } else {
                Toast.makeText(
                    this,
                    "Display over app permission was not granted",
                    Toast.LENGTH_LONG
                ).show()
            }
        }
        super.onActivityResult(requestCode, resultCode, resultData)
    }

    override val activityLayout: Int
        get() = R.layout.activity_dashboard

    override fun onBackPressed() {
    }

    companion object {
        private const val REQUEST_SETTINGS = 7654
    }

}
