/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(
    tableName = "user_data",
    foreignKeys = [ForeignKey(
        entity = Service::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("service_id"),
        onDelete = ForeignKey.CASCADE
    )]
)

data class UserData(
    var title: String,
    var type: String,
    @field:ColumnInfo(name = "value")
    var subtype: String,
    @field:ColumnInfo(name = "concatenatedData")
    var value: String,
    @field:ColumnInfo(name = "service_id", index = true) var serviceId: Long,
    var date: Long,
    var parentId: String? = null,
) : java.io.Serializable {

    @PrimaryKey
    var id: String = java.util.UUID.randomUUID().toString()

    val unConcatenatedData: Array<String>
        get() = value.split(DELIMITER).toTypedArray()

    fun isValid(): Boolean {
        if (subtype.isEmpty() || subtype.isBlank() || subtype == "null")
            return false
        if (value == "null")
            return false
        return true
    }

    companion object {
        const val DELIMITER = "@/:-"
    }

    @Ignore
    var hasChildren: Boolean = false
}
