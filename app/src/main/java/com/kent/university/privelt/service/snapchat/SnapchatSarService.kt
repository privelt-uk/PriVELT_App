package com.kent.university.privelt.service.snapchat

import android.content.Context
import com.google.gson.Gson
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.model.snapchat.*
import com.kent.university.privelt.service.base.DataTypeList
import com.kent.university.privelt.service.base.SARService
import com.kent.university.privelt.service.base.State
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.InputStream
import java.util.zip.ZipInputStream

class SnapchatSarService: SARService() {

    override val sarRequestURL: String = "https://help.snapchat.com/hc/en-gb/articles/7012305371156-How-do-I-download-my-data-from-Snapchat"
    override val service: Service = Service("Snapchat SAR", false, "", "", "")

    override fun parseSAR(context: Context, inputStream: InputStream) =
        CoroutineScope(Dispatchers.IO).launch {
            _stateFlow.emit(State.Loading)
            val date = System.currentTimeMillis()
            val zipInputStream = ZipInputStream(inputStream)
            val pairs = zipInputStream.use { zipStream ->
                generateSequence { zipStream.nextEntry }
                    .filter { it.name.endsWith(".json") }
                    .filterNot { it.name.startsWith("__") }
                    .map {
                        Pair(it.name, zipStream.reader().readText())
                    }
                    .toList()
            }
            val gson = Gson()
            val serviceId = 0L
            if (pairs.isEmpty()) {
                _stateFlow.emit(State.Error("The selected file is not a SAR file for this service"))
                return@launch
            }
            pairs.map {
                if (it.first.contains("account.json")) {
                    val userData = gson.fromJson(it.second, SnapchatBasicInformation::class.java).toUserData(serviceId, date)
                    saveUserDataToRepository(context, userData)
                } else if (it.first.contains("account_history.json")) {
                    val userData = gson.fromJson(it.second, SnapchatAccountActivity::class.java).toUserData(serviceId, date)
                    saveUserDataToRepository(context, userData)
                } else if (it.first.contains("connected_apps.json")) {
                    val userData = gson.fromJson(it.second, SnapchatConnectedApps::class.java).toUserData(serviceId, date)
                    saveUserDataToRepository(context, userData)
                } else if (it.first.contains("location_history.json")) {
                    val userData = gson.fromJson(it.second, SnapchatLocationData::class.java).toUserData(serviceId, date)
                    saveUserDataToRepository(context, userData)
                } else if (it.first.contains("snap_pro.json")) {
                    val userData = gson.fromJson(it.second, SnapchatProfile::class.java).toUserData(serviceId, date)
                    saveUserDataToRepository(context, userData)
                } else if (it.first.contains("user_profile.json")) {
                    val userData = gson.fromJson(it.second, SnapchatUserProfile::class.java).toUserData(serviceId, date)
                    saveUserDataToRepository(context, userData, true)
                } else if (it.first.contains("account.json")) {
                    val userData = gson.fromJson(it.second, SnapchatData::class.java).toUserData(serviceId, date)
                    saveUserDataToRepository(context, userData)
                }
            }
        }
}