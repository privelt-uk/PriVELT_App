package com.kent.university.privelt.ui.risk_value

import android.content.res.ColorStateList

/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.core.widget.CompoundButtonCompat
import androidx.recyclerview.widget.RecyclerView
import com.kent.university.privelt.databinding.CellSensorCheckboxBinding
import com.kent.university.privelt.events.CheckedSensorEvent
import com.kent.university.privelt.utils.getRandomColor
import org.greenrobot.eventbus.EventBus
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap

class CardChartAdapter internal constructor(
    private val cards: List<String>,
    alreadyChecked: List<String>,
    val checkedCard:(List<String>) -> Unit
) : RecyclerView.Adapter<CardChartAdapter.CardChartViewHolder>() {

    private val cardsChecked: LinkedHashMap<String?, Boolean> = LinkedHashMap()
    private val colors: MutableList<Int> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardChartAdapter.CardChartViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = CellSensorCheckboxBinding.inflate(inflater, parent, false)
        return CardChartAdapter.CardChartViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CardChartAdapter.CardChartViewHolder, position: Int) {
        holder.bind(cards[position], cardsChecked, checkedCard, colors[position])
    }

    override fun getItemCount(): Int {
        return cardsChecked.size
    }

    fun getColor(name: String): Int {
        val position = cards.indexOf(name)
        return colors[position]
    }

    init {
        for (script in cards) {
            this.cardsChecked[script] = false
            Random()
            val color = getRandomColor()
            colors.add(color)
        }
        for (script in alreadyChecked) {
            if (script.isNotEmpty() && this.cardsChecked.containsKey(script))
                this.cardsChecked[script] = true
        }
    }

    class CardChartViewHolder internal constructor(private val binding: CellSensorCheckboxBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(
            sensor: String?,
            scripts: LinkedHashMap<String?, Boolean>,
            checkedCard: (List<String>) -> Unit,
            color: Int
        ) {
            binding.sensorCheckbox.text = sensor
            binding.sensorCheckbox.isChecked = scripts[sensor]!!
            binding.sensorCheckbox.setTextColor(
                color
            )
            CompoundButtonCompat.setButtonTintList(
                binding.sensorCheckbox,
                ColorStateList.valueOf(
                    color
                )
            )
            binding.sensorCheckbox.setOnCheckedChangeListener { _: CompoundButton?, b: Boolean ->

                fun getSelectedSensors(): List<String> {
                    val list = ArrayList<String>()
                    scripts.forEach {
                        if (it.value)
                            list.add(it.key!!)
                    }
                    return list
                }

                scripts[sensor] = b
                if (scripts.containsValue(true)) {
                    EventBus.getDefault().post(CheckedSensorEvent())
                } else {
                    scripts[sensor] = true
                    binding.sensorCheckbox.isChecked = true
                }
                checkedCard.invoke(getSelectedSensors())
            }
            //itemView.logo.setImageResource(sensor.resId)
        }



    }
}


