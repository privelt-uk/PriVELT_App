/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.base

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.kent.university.privelt.PriVELTApplication
import com.kent.university.privelt.R
import com.kent.university.privelt.api.PasswordManager
import com.kent.university.privelt.di.DaggerPriVELTComponent
import com.kent.university.privelt.di.RoomModule
import com.kent.university.privelt.utils.firebase.RemoteConfigHelper

abstract class BaseActivity : AppCompatActivity() {

    protected abstract val activityLayout: Int
    protected abstract fun configureViewModel()
    protected abstract fun configureDesign(savedInstanceState: Bundle?)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Associates the layout file to this class
        setContentView(activityLayout)
        configureViewModel()
        configureDesign(savedInstanceState)
    }

    protected fun <T : ViewModel> getViewModel(className: Class<T>): T {
        // Component
        val component = DaggerPriVELTComponent.builder().roomModule(RoomModule(this)).build()
        // ViewModelFactory
        val factory = component.viewModelFactory
        return ViewModelProvider(this, factory!!)[className]
    }

    protected val identityManager: PasswordManager? get() = (application as PriVELTApplication).identityManager

    override fun onResume() {
        super.onResume()
        (applicationContext as PriVELTApplication).currentActivity = this
        setCurrentScreen()
    }

    private val helpItemId = 123

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val helpItem: MenuItem = menu.add(0, helpItemId, 0, "Help")
        helpItem.icon = ResourcesCompat.getDrawable(resources, R.drawable.ic_help, null)
        helpItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        if (item.itemId == helpItemId) {
            val parentLayout = findViewById<View>(android.R.id.content)
            when (RemoteConfigHelper.getHelpType()) {
                1 -> {
                    val snackBar: Snackbar = Snackbar.make(
                        parentLayout,
                        RemoteConfigHelper.getHelpContent(),
                        Snackbar.LENGTH_LONG
                    )
                    val snackBarView = snackBar.view
                    val snackTextView =
                        snackBarView.findViewById<View>(R.id.snackbar_text) as TextView
                    snackTextView.maxLines = 15
                    snackBar.show()
                }
                2 -> {
                    Toast.makeText(this, RemoteConfigHelper.getHelpContent(), Toast.LENGTH_LONG)
                        .show()
                }
                4 -> {
                    AlertDialog.Builder(this, R.style.AlertTheme)
                        .setTitle("Help")
                        .setMessage(RemoteConfigHelper.getHelpContent())
                        .setIcon(android.R.drawable.ic_dialog_info)
                        .show()
                }
            }
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setCurrentScreen() {
        when (activityLayout) {
            R.layout.activity_invitation -> RemoteConfigHelper.nextScreenActivity("1.0.3", "activity_invitation")
            R.layout.activity_barchart -> RemoteConfigHelper.nextScreenActivity("2.2.3", "activity_barchart")
            R.layout.activity_dashboard -> RemoteConfigHelper.nextScreenActivity("2.0.0", "activity_dashboard")
            R.layout.activity_data -> RemoteConfigHelper.nextScreenActivity("2.0.7", "activity_data")
            R.layout.activity_detailed_card -> RemoteConfigHelper.nextScreenActivity("2.0.4", "activity_detailed_card")
            R.layout.activity_detailed_sensor -> RemoteConfigHelper.nextScreenActivity("3.0.1", "activity_detailed_sensor")
            R.layout.activity_first_screen -> RemoteConfigHelper.nextScreenActivity("1.0.0", "activity_first_screen")
            R.layout.activity_login -> RemoteConfigHelper.nextScreenActivity("2.0.2", "activity_login")
            R.layout.activity_main_password -> RemoteConfigHelper.nextScreenActivity("1.0.1", "activity_main_password")
            R.layout.activity_privacy -> RemoteConfigHelper.nextScreenActivity("2.0.8", "activity_privacy")
            R.layout.activity_risk_value_service -> RemoteConfigHelper.nextScreenActivity("2.1.0", "activity_risk_value_service")
            R.layout.activity_risk_value_permission -> RemoteConfigHelper.nextScreenActivity("3.1.0", "activity_risk_value_permission")
            R.layout.activity_sensor_chart -> RemoteConfigHelper.nextScreenActivity("3.0.2", "activity_sensor_chart")
            R.layout.activity_api_service -> RemoteConfigHelper.nextScreenActivity("", "activity_api_service")
            R.layout.activity_sar_service -> RemoteConfigHelper.nextScreenActivity("", "activity_sar_service")
            R.layout.fragment_settings -> RemoteConfigHelper.nextScreenActivity("5.0.0", "fragment_settings")
        }
    }

}
