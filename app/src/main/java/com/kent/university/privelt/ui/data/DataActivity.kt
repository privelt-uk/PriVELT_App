/*
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  * License, v. 2.0. If a copy of the MPL was not distributed with this
 *  * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */
package com.kent.university.privelt.ui.data

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kent.university.privelt.R
import com.kent.university.privelt.base.BaseActivity
import com.kent.university.privelt.databinding.ActivityDataBinding
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.model.UserData
import com.kent.university.privelt.ui.login.LoginActivity
import com.kent.university.privelt.ui.login.LoginActivity.Companion.PARAM_SERVICE
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume


class DataActivity : BaseActivity() {

    private lateinit var binding: ActivityDataBinding

    private var serviceId: Long = 0

    private var service: String? = null

    private var type: String? = null

    private var parentId: String? = null

    private var dataViewModel: DataViewModel? = null

    private var dataAdapter: DataAdapter? = null

    override fun configureDesign(savedInstanceState: Bundle?) {
        binding = ActivityDataBinding.inflate(layoutInflater)
        setContentView(binding.root)
        if (savedInstanceState != null) {
            service = savedInstanceState.getString(LoginActivity.PARAM_SERVICE)
            type = savedInstanceState.getString(PARAM_TYPE)
            parentId = savedInstanceState.getString(PARAM_PARENT_ID)
        } else if (intent != null) {
            service = intent.getStringExtra(LoginActivity.PARAM_SERVICE)
            type = intent.getStringExtra(PARAM_TYPE)
            parentId = intent.getStringExtra(PARAM_PARENT_ID)
        }

        if (parentId != null) {
            if (type != null)
                title = type?.capitalize()
            else if (service != null)
                title = service?.capitalize()
        }
        configureRecyclerView()
        getServices()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(LoginActivity.PARAM_SERVICE, service)
        outState.putSerializable(PARAM_TYPE, type)
    }

    private fun configureRecyclerView() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this)
        binding.recyclerViewUserdata.layoutManager = layoutManager
        dataAdapter = DataAdapter(ArrayList(), parentId == null, {
            isPossibleToGoDeeper(it)
        }, { userDataLongClicked, view ->
            val popupMenu = PopupMenu(this@DataActivity, view)

            popupMenu.menuInflater.inflate(R.menu.user_data_long_click_menu, popupMenu.getMenu())
            popupMenu.setOnMenuItemClickListener {
                if (it.itemId == R.id.copy) {
                    val text = if (userDataLongClicked.value.isNotEmpty())
                        "${userDataLongClicked.subtype}\n${userDataLongClicked.value}"
                    else
                        userDataLongClicked.subtype
                    val clipboard: ClipboardManager =
                        getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
                    val clip: ClipData = ClipData.newPlainText("PriVELT", text)
                    clipboard.setPrimaryClip(clip)
                    
                }
                true
            }
            popupMenu.show()
        })
        binding.recyclerViewUserdata.adapter = dataAdapter
    }

    private suspend fun hasChildren(userDataParam: UserData): Boolean? = suspendCancellableCoroutine { continuation ->
        dataViewModel!!.getUserDataParentId(serviceId, userDataParam.type, userDataParam.id)
            .observe(this@DataActivity) { userData: List<UserData> ->
                continuation.resume(userData.isNotEmpty())
            }
    }

    private fun isPossibleToGoDeeper(userDataParam: UserData) = CoroutineScope(Dispatchers.Main).launch {
        dataViewModel!!.getUserDataParentId(serviceId, userDataParam.type, userDataParam.id)
            .observe(this@DataActivity) { userData: List<UserData> ->
                val isPossible = userData.isNotEmpty()
                if (isPossible) {
                    val intent = Intent(this@DataActivity, DataActivity::class.java)
                    intent.putExtra(PARAM_PARENT_ID, userDataParam.id)
                    intent.putExtra(PARAM_TYPE, userDataParam.type)
                    intent.putExtra(PARAM_SERVICE, service)
                    startActivity(intent)
                }
            }
    }

    private fun getServices() {
        dataViewModel!!.services?.observe(this) { services: List<Service> -> updateServices(services) }
    }

    private fun getUserData(id: Long, type: String?, parentId: String?) {
        dataViewModel!!.getUserDataParentId(id, type, parentId)
            .observe(this) { userData: List<UserData> ->
                updateUserData(userData)
            }
    }

    private fun updateServices(services: List<Service>) {
        var id: Long = -1
        for (service in services)
            if (service.name == this.service)
                id = service.id
        serviceId = id
        getUserData(id, type, parentId)
    }

    private fun updateUserData(userData: List<UserData>) = CoroutineScope(Dispatchers.Main).launch {
        userData.forEach {
            it.hasChildren = hasChildren(it) ?: false
        }
        if (userData.isEmpty()) {
            binding.progressLayout.visibility = View.VISIBLE
        } else {
            dataAdapter!!.setUserData(userData)
            dataAdapter!!.notifyDataSetChanged()
            binding.progressLayout.visibility = View.GONE
        }
    }

    override val activityLayout: Int
        get() = R.layout.activity_data

    override fun configureViewModel() {
        dataViewModel = getViewModel(DataViewModel::class.java)
        dataViewModel?.init()
    }

    companion object {
        const val PARAM_TYPE = "PARAM_TYPE"
        const val PARAM_PARENT_ID = "PARAM_PARENT_ID"
    }

}
