package com.kent.university.privelt.service.expedia

import android.content.Context
import com.kent.university.privelt.model.Service
import com.kent.university.privelt.service.base.SARService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.InputStream

class ExpediaService : SARService() {

    override var sarRequestURL: String = "https://www.expedia.co.uk/service/#/articles/991/1019/22891"
    override val service = Service("Expedia", false, "", "", "")

    override fun parseSAR(context: Context, inputStream: InputStream) =
        CoroutineScope(Dispatchers.IO).launch {
            // TODO: Parse The SAR File!
        }

}
