package com.kent.university.privelt.ui.data

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.kent.university.privelt.R
import com.kent.university.privelt.model.UserData
import com.unnamed.b.atv.model.TreeNode

class DataTreeViewHolder(context: Context, private val clickCallback: (UserData, View) -> Unit): TreeNode.BaseNodeViewHolder<UserData>(context) {
    override fun createNodeView(node: TreeNode, userData: UserData): View {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val view: View = inflater.inflate(R.layout.layout_child_node, null, false)
        val title: TextView = view.findViewById<View>(R.id.title) as TextView
        val value: TextView = view.findViewById<View>(R.id.subtitle) as TextView
        title.text = userData.subtype
        value.text = userData.value

        view.setOnLongClickListener {
            clickCallback.invoke(userData, it)
            true
        }
        return view
    }
}